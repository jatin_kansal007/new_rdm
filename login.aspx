﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RDM | User Login</title>
    <meta charset="utf-8" />
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="keywords" />

    <!-- Favicons -->
    <link href="assets/img/favicon.png" rel="icon" />
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon" />

    <!-- Google Fonts -->
    <link href="https://fonts.gstatic.com" rel="preconnect" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet" />

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet" />
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet" />
    <link href="assets/vendor/quill/quill.snow.css" rel="stylesheet" />
    <link href="assets/vendor/quill/quill.bubble.css" rel="stylesheet" />
    <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet" />
    <link href="assets/vendor/simple-datatables/style.css" rel="stylesheet" />

    <!-- Template Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet" />
    <link href="css/loginstyle.css" rel="stylesheet" />
    <link href="css/loader.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="loginbody">
            <div class="card wrap-login100 p-l-110 p-r-110 p-t-62 p-b-33 ">
                <h2 class="text-center">Sign In</h2>
                <div class=" align-items-center  text-center">
                    <img class="w-100" src="assets/img/rdmlogo.png" />
                    <img class="loginimg w-100" src="assets/img/undraw_secure_login_pdn4.svg" />

                </div>
                  <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="InputMobile" placeholder=""/>
                        <label for="InputMobile">Mobile</label>
                    </div>
                  <div class="form-floating mb-3">
                        <input type="password" class="form-control" id="InputPassword" placeholder=""/>
                        <label for="InputPassword">Password</label>
                    </div>
                <%--<div class="mb-2">
                    <label for="InputEmail1" class="form-label">Mobile</label>
                    <input type="text" class="form-control" id="InputMobile" aria-describedby="emailHelp" />

                </div>--%>
              <%--  <div class="mb-2">
                    <label for="exampleInputPassword1" class="form-label">Password</label>
                    <input type="password" class="form-control" id="InputPassword" />
                </div>--%>

                <div id="captchaarea">
                    <div class="row">
                        <div class="col-sm-7">
                            <div>
                                <input type="text" readonly="" id="generated-captcha" />
                                <box-icon onclick="generate()" id="refreshbtn" style="position: absolute;right: 50px;margin-top: 10px;" name='refresh'></box-icon>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <button type="button" onclick="generate()" id="gen">
                                Generate New
                            </button>
                        </div>
                    </div>

                    <div class="mb-2" style="margin-top: -19px;">
                        <label for="exampleInputPassword1" class="form-label"></label>
                        <input type="text" class="form-control" id="entered-captcha" autocomplete="off" placeholder="Enter the captcha.." />
                    </div>

                   
                    <%--<button type="button" onclick="check()">
                    check
                </button>--%>
                </div>


                <button id="Signinbtn" onclick="CheckCredentials()" runat="server" type="button" class="btn btn-primary"><i class="bi bi-box-arrow-in-right"></i>Sign in now</button>
            </div>
        </div>
    </form>

    <!-- Vendor JS Files -->
    <script src="assets/vendor/apexcharts/apexcharts.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/chart.js/chart.min.js"></script>
    <script src="assets/vendor/echarts/echarts.min.js"></script>
    <script src="assets/vendor/quill/quill.min.js"></script>
    <script src="assets/vendor/simple-datatables/simple-datatables.js"></script>
    <script src="assets/vendor/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="https://unpkg.com/nprogress@0.2.0/nprogress.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>
    <script src="https://unpkg.com/boxicons@2.1.1/dist/boxicons.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>
    <script src="jquery/loader.js"></script>
    <script src="jquery/loginhelper.js?v=0.1"></script>
    <script src="jquery/Main.js"></script>
</body>
</html>
