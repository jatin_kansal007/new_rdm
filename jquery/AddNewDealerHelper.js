﻿$("#OpenDownBtn").click(function (e) {
    e.stopImmediatePropagation();
    e.preventDefault();
    
    $(this).hide();

   if ($(".FormBody__").hasClass("close")) {
       $(".FormBody__").slideDown('slow');
       $(".FormBody__").removeClass("close");
       $(".FormBody__").addClass("open");
        $("#Distributor_submitbtn").show('slow');
        $("#Cancelbtn").show('slow');
    } else {
       $(".FormBody__").slideUp('slow');
       $(".FormBody__").addClass("close");
       $(".FormBody__").removeClass("open");
        $("#Distributor_submitbtn").hide('slow');
        $("#Cancelbtn").hide('slow');
    }

});




$(function () {
    $('#Alluploadeddata').DataTable({
    });
});

$(".form-header").click(function (e) {
    e.stopImmediatePropagation();
    e.preventDefault();
    $(this).parent().find(".form-body").slideToggle();
    if ($(this).find('.angles i').hasClass('bi-caret-down-fill')) {
        $(this).find('.angles').empty();
        $(this).find('.angles').append('<i class="bi bi-caret-up-fill"></i>');
    } else {
        $(this).find('.angles').empty();
        $(this).find('.angles').append('<i class="bi bi-caret-down-fill"></i>');
    }

});