﻿$(function () {
    //GetTotalCountsofData();
    //processdata();

    getPrcessData();
});

function GetTotalCountsofData() {
    $.ajax({
        type: "POST",
        async: true,
        url: "/Admin/Dashboard.aspx/methods_getcounts",
        data: {},
        beforeSend: function () {
            NProgress.start();

        },
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            if (response.d != "") {
                $("#Total_sim_count").text(response.d);
            }
        },
        error: function (response) {
            swal({
                title: "oops!",
                text: response.responseText,
                icon: "warning",
            });
        },
        complete: function () {
            NProgress.done();
        },
    });
}

$("#fileupload__btn").click(function (e) {
    e.stopImmediatePropagation();
    e.preventDefault();
    var fileUpload = $("#formFile")[0];
    if ($("#formFile").val().toLowerCase().indexOf(".xlsx") > 0) {
        /*if ($("#formFile").val().toLowerCase().indexOf(".csv") > 0) {*/
        if (typeof (FileReader) != "undefined") {
            var reader = new FileReader();

            //For Browsers other than IE.
            if (reader.readAsBinaryString) {
                reader.onload = function (e) {
                    ProcessExcel(e.target.result);
                };
                reader.readAsBinaryString(fileUpload.files[0]);
            } else {
                //For IE Browser.
                reader.onload = function (e) {
                    var data = "";
                    var bytes = new Uint8Array(e.target.result);
                    for (var i = 0; i < bytes.byteLength; i++) {
                        data += String.fromCharCode(bytes[i]);
                    }
                    ProcessExcel(data);
                };
                reader.readAsArrayBuffer(fileUpload.files[0]);
            }
        } else {
            swal({
                title: "oops!",
                text: "This browser does not support HTML5.",
                icon: "warning",
            });

        }
    } else {

        swal({
            title: "oops!",
            text: "Please upload a valid Excel file.",
            icon: "warning",
        });
    }
});

function ProcessExcel(data) {

    //Read the Excel File data.
    var workbook = XLSX.read(data, {
        type: 'binary'
    });

    //Fetch the name of First Sheet.d
    var firstSheet = workbook.SheetNames[0];

    //Read all rows from First Sheet into an JSON array.
    var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
    console.log(excelRows);
    //http://localhost:59900/WebService1.asmx


    var Result = excelRows.length;

    var Loop = (Result / 1000).toFixed(0);
    if (Loop == '0') {
        Loop = '1';
    }

    for (var i = 0; i < Loop; i++) {

        var DataExcel = excelRows.slice(parseInt(i + '000'), parseInt(i + '999'));

        var Original_File_Name = $("#formFile").val();
        Original_File_Name = Original_File_Name.replace(/^.*\\/, "").replace('.', '|');
        var data2 = '{data:' + JSON.stringify(DataExcel) + ',Original_File_Name:"' + Original_File_Name + '"}';
        $.ajax({
            type: "POST",
            async: true,
            url: "/Admin/Dashboard.aspx/method_upload_excel_file",
            data: data2,
            beforeSend: function () {
                NProgress.start();
                loader();
            },
            contentType: "application/json",
            dataType: "json",
            success: function (response) {
                if (response.d != "") {
                    swal({
                        title: "Yehh!",
                        text: response.d,
                        icon: "success",
                    });
                    DataExcel = [];
                    $("#processdata__btn").removeAttr("disabled");
                }
            },
            error: function (response) {
                swal({
                    title: "oops!",
                    text: response.responseText,
                    icon: "warning",
                });
            },
            complete: function () {
                NProgress.done();
            },
        });
    }

};

//$("#fileupload__btn").click(function (e) {
//    e.stopImmediatePropagation();
//    e.preventDefault();
//    var fileUpload = $(this).attr('url');
//    fileUpload = (fileUpload).replace(/^data:.csv,application\/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,/, "");
//     fileUpload = (fileUpload).replace(/^data:application\/vnd.ms-excel;base64,/, "")

//    var data2 = JSON.stringify({ data: fileUpload });
//    $.ajax({
//        type: "POST",
//        async: true,
//        url: "Admin/Dashboard.aspx//method_frouploadfile",
//        data: data2,
//        beforeSend: function () {
//            NProgress.start();

//        },
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (response) {
//            if (response.d != "") {
//                swal({
//                    title: "Yehh!",
//                    text: response.d,
//                    icon: "success",
//                });
//                DataExcel = [];
//            }


//        },
//        error: function (response) {
//            swal({
//                title: "oops!",
//                text: response.responseText,
//                icon: "warning",
//            });
//        },
//        complete: function () {
//            NProgress.done();
//        },
//    });

//});

//$("#formFile").change(function (g) {
//    g.preventDefault();
//    g.stopImmediatePropagation();
//        var reader = new FileReader();
//        reader.onload = imageIsLoaded;
//        reader.readAsDataURL(this.files[0]);

//});
//function imageIsLoaded(e) {
//    $("#fileupload__btn").attr('url', e.target.result);
//};


function processdata() {
    $.ajax({
        type: "POST",
        async: true,
        url: "/Admin/Dashboard.aspx/method_process_upload_excel_file",
        data: {},
        beforeSend: function () {
            NProgress.start();
            loader();
        },
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            if (response.d != "") {
                swal({
                    title: "Yehh!",
                    text: "Data processed successfully.",
                    icon: "success",
                });
                var data = response.d.split('~');

                $("#newrecorddisplay").text(data[2]);
                $("#mismatchrecorddisplay").text(data[1] != "" ? data[1] : 0);
                $("#samerecorddisplay").text(data[0]);
                $("#mismatchdataget").show();
                $("#appendsamerecord").show();
                $("#viewnewrecord").show();
            }


        },
        error: function (response) {
            swal({
                title: "oops!",
                text: response.responseText,
                icon: "warning",
            });
        },
        complete: function () {
            NProgress.done();
            hideloader();
        },
    });
}


$("#processdata__btn").click(function (e) {
    e.stopImmediatePropagation();
    e.preventDefault();
    processdata();
    fn_getuploadeddata();
    $("#table_card").show();
});

function getDataTable(name) {

}

function fn_getuploadeddata() {
    $("#cardtitle").text("All Uploaded Records");
    $.ajax({
        type: "POST",
        async: true,
        url: "/Admin/Dashboard.aspx/method_getuploadeddata",
        data: '{foruse:"alldata"}',
        beforeSend: function () {
            NProgress.start();
        },
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            if (response.d != "") {
                var parsedData = JSON.parse(response.d);

                if ($.fn.dataTable.isDataTable('#Alluploadeddata')) {
                    table = $('#Alluploadeddata').DataTable({
                        "order": [[1, "desc"]],
                        data: parsedData,
                        stateSave: true,
                        "bDestroy": true,
                        "iDisplayLength": 10,
                        "aLengthMenu": [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                        columns: [
                            {
                                render: function (data, type, row) {
                                    return row.SIMNO;
                                }
                            },
                            {
                                render: function (data, type, row) {
                                    return row.CARDSTATE;
                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.CARDSTATUS;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.CUSTOMERNAME;


                                }

                            },

                            {
                                render: function (data, type, row) {
                                    return row.ACCOUNTNO;


                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.ORDERNO;



                                }
                            },
                            {
                                render: function (data, type, row) {

                                    return row.PRODUCT;



                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.PROJECT;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.SMSUSAGE;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.DATAUSAGE;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.IMEI;


                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPPRIMARYIMSI;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPPRIMARYTSP;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPPRIMARYMSISDN;

                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.BOOTSTRAPPRIMARYSUBSCRIPTIONSTATUS;





                                }
                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPFALLBACKIMSI;

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPFALLBACKTSP;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPFALLBACKMSISDN;

                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.BOOTSTRAPFALLBACKSUBSCRIPTIONSTATUS;

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.DATEOFCHANGEOVERTOCOMMERCIALPLAN;


                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.CARDENDDATE;



                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.COMMERCIALPRIMARYIMSI;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.COMMERCIALPRIMARYTSP;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.COMMERCIALPRIMARYMSISDN;

                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.COMMERCIALPRIMARYSUBSCRIPTIONSTATUS;




                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.LASTSRNUMBER;

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.LASTSRACTION;



                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.LASTSRDATE;

                                }

                            }
                        ],
                        "orderClasses": false,
                        responsive: true,
                        dom: 'lBfrtip',


                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }
                else {
                    table = $('#Alluploadeddata').DataTable({
                        "order": [[1, "desc"]],
                        data: parsedData,
                        stateSave: true,
                        "bDestroy": true,
                        "iDisplayLength": 10,
                        "aLengthMenu": [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                        columns: [
                            {
                                render: function (data, type, row) {
                                    return row.SIMNO;
                                }
                            },
                            {
                                render: function (data, type, row) {
                                    return row.CARDSTATE;
                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.CARDSTATUS;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.CUSTOMERNAME;


                                }

                            },

                            {
                                render: function (data, type, row) {
                                    return row.ACCOUNTNO;


                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.ORDERNO;



                                }
                            },
                            {
                                render: function (data, type, row) {

                                    return row.PRODUCT;



                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.PROJECT;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.SMSUSAGE;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.DATAUSAGE;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.IMEI;


                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPPRIMARYIMSI;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPPRIMARYTSP;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPPRIMARYMSISDN;

                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.BOOTSTRAPPRIMARYSUBSCRIPTIONSTATUS;





                                }
                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPFALLBACKIMSI;

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPFALLBACKTSP;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPFALLBACKMSISDN;

                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.BOOTSTRAPFALLBACKSUBSCRIPTIONSTATUS;

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.DATEOFCHANGEOVERTOCOMMERCIALPLAN;


                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.CARDENDDATE;



                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.COMMERCIALPRIMARYIMSI;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.COMMERCIALPRIMARYTSP;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.COMMERCIALPRIMARYMSISDN;

                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.COMMERCIALPRIMARYSUBSCRIPTIONSTATUS;




                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.LASTSRNUMBER;

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.LASTSRACTION;



                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.LASTSRDATE;

                                }

                            }
                        ],
                        "orderClasses": false,
                        responsive: true,
                        dom: 'lBfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }

                $("#processdata__btn").removeAttr('disabled');
                /* $("#viewalluploaddata").show();*/

            }
        },
        error: function (response) {
            swal({
                title: "oops!",
                text: response.responseText,
                icon: "warning",
            });
        },
        complete: function () {
            NProgress.done();
        },
    });
}

$("#appendsamerecord").click(function (e) {
    e.stopImmediatePropagation();
    e.preventDefault();
    $("#cardtitle").text("Same Records");
    $.ajax({
        type: "POST",
        async: true,
        url: "/Admin/Dashboard.aspx/method_getuploadeddata",
        data: '{foruse:"samedata"}',
        beforeSend: function () {
            NProgress.start();
            loader();
        },
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            if (response.d != "") {

                var parsedData = JSON.parse(response.d);
                $("#Processbtn").removeAttr("btnfor");
                $("#Processbtn").attr("disabled", "disabled");
                $("#Processbtnallnewrecordbtn").attr("disabled", "disabled");
                $("#rejectselectedbtn").attr("disabled", "disabled");

                if ($.fn.dataTable.isDataTable('#Alluploadeddata')) {
                    table = $('#Alluploadeddata').DataTable({
                        "bSort": true,
                        data: parsedData,
                        stateSave: true,
                        "bDestroy": true,
                        "iDisplayLength": 10,
                        "aLengthMenu": [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                        columns: [
                            {
                                render: function (data, type, row) {
                                    return row.SIMNO;
                                }
                            },
                            {
                                render: function (data, type, row) {
                                    return row.CARDSTATE;
                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.CARDSTATUS;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.CUSTOMERNAME;


                                }

                            },

                            {
                                render: function (data, type, row) {
                                    return row.ACCOUNTNO;


                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.ORDERNO;



                                }
                            },
                            {
                                render: function (data, type, row) {

                                    return row.PRODUCT;



                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.PROJECT;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.SMSUSAGE;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.DATAUSAGE;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.IMEI;


                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPPRIMARYIMSI;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPPRIMARYTSP;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPPRIMARYMSISDN;

                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.BOOTSTRAPPRIMARYSUBSCRIPTIONSTATUS;





                                }
                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPFALLBACKIMSI;

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPFALLBACKTSP;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPFALLBACKMSISDN;

                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.BOOTSTRAPFALLBACKSUBSCRIPTIONSTATUS;

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.DATEOFCHANGEOVERTOCOMMERCIALPLAN;


                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.CARDENDDATE;



                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.COMMERCIALPRIMARYIMSI;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.COMMERCIALPRIMARYTSP;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.COMMERCIALPRIMARYMSISDN;

                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.COMMERCIALPRIMARYSUBSCRIPTIONSTATUS;




                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.LASTSRNUMBER;

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.LASTSRACTION;



                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.LASTSRDATE;

                                }

                            }
                        ],
                        "orderClasses": false,
                        responsive: true,
                        dom: 'lBfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }
                else {
                    table = $('#Alluploadeddata').DataTable({

                        "bSort": true,
                        data: parsedData,
                        stateSave: true,
                        "bDestroy": true,
                        "iDisplayLength": 10,
                        "aLengthMenu": [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                        columns: [
                            {
                                render: function (data, type, row) {
                                    return row.SIMNO;
                                }
                            },
                            {
                                render: function (data, type, row) {
                                    return row.CARDSTATE;
                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.CARDSTATUS;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.CUSTOMERNAME;


                                }

                            },

                            {
                                render: function (data, type, row) {
                                    return row.ACCOUNTNO;


                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.ORDERNO;



                                }
                            },
                            {
                                render: function (data, type, row) {

                                    return row.PRODUCT;



                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.PROJECT;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.SMSUSAGE;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.DATAUSAGE;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.IMEI;


                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPPRIMARYIMSI;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPPRIMARYTSP;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPPRIMARYMSISDN;

                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.BOOTSTRAPPRIMARYSUBSCRIPTIONSTATUS;





                                }
                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPFALLBACKIMSI;

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPFALLBACKTSP;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPFALLBACKMSISDN;

                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.BOOTSTRAPFALLBACKSUBSCRIPTIONSTATUS;

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.DATEOFCHANGEOVERTOCOMMERCIALPLAN;


                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.CARDENDDATE;



                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.COMMERCIALPRIMARYIMSI;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.COMMERCIALPRIMARYTSP;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.COMMERCIALPRIMARYMSISDN;

                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.COMMERCIALPRIMARYSUBSCRIPTIONSTATUS;




                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.LASTSRNUMBER;

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.LASTSRACTION;



                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.LASTSRDATE;

                                }

                            }
                        ],
                        "orderClasses": false,
                        responsive: true,
                        dom: 'lBfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }

            }
            $("#table_card").show();
        },
        error: function (response) {
            swal({
                title: "oops!",
                text: response.responseText,
                icon: "warning",
            });
        },
        complete: function () {
            NProgress.done();
            hideloader();
        },
    });

});

$("#mismatchdataget").click(function (e) {
    e.stopImmediatePropagation();
    e.preventDefault();
    $("#cardtitle").text("Mismatch Records");
    getmismatchdata();
    $("#Processbtn").show();
    $("#Processbtnallnewrecordbtn").show();
    $("#rejectselectedbtn").show();

});

function getmismatchdata() {
    $.ajax({
        type: "POST",
        async: true,
        url: "/Admin/Dashboard.aspx/method_getuploadeddata",
        data: '{foruse:"mismatchdata"}',
        beforeSend: function () {
            NProgress.start();
            loader();
        },
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            if (response.d != "") {
                $("#Processbtn").removeAttr("disabled");
                $("#Processbtnallnewrecordbtn").removeAttr("disabled");
                $("#rejectselectedbtn").removeAttr("disabled");
                $("#Processbtn").removeAttr("btnfor");
                $("#Processbtn").attr("btnfor", "mismatchprocess");
                var parsedData = JSON.parse(response.d);

                if ($.fn.dataTable.isDataTable('#Alluploadeddata')) {
                    table = $('#Alluploadeddata').DataTable({
                        "order": [[1, "desc"]],
                        data: parsedData,
                        stateSave: true,
                        "bDestroy": true,
                        "iDisplayLength": 10,
                        "aLengthMenu": [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                        columns: [
                            //{
                            //    render: function (data, type, row) {
                            //        return row.SRNO;
                            //    }

                            //},
                            {
                                render: function (data, type, row) {
                                    var data = row.SIMNO.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }
                                    return "<div class='orig" + cls_original + "'>" + data[0] + "</div><div class='mismatchrecordrow" + cls + "'><input type='checkbox' class='table2_simno'/> &nbsp;" + data[1] + "</div>";
                                }
                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.CARDSTATE.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.CARDSTATUS.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.CUSTOMERNAME.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },

                            {
                                render: function (data, type, row) {
                                    var data = row.ACCOUNTNO.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.ORDERNO.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }
                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.PRODUCT.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.PROJECT.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.SMSUSAGE.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.DATAUSAGE.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.IMEI.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.BOOTSTRAPPRIMARYIMSI.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.BOOTSTRAPPRIMARYTSP.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.BOOTSTRAPPRIMARYMSISDN.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {
                                    var data = row.BOOTSTRAPPRIMARYSUBSCRIPTIONSTATUS.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";


                                }
                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.BOOTSTRAPFALLBACKIMSI.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.BOOTSTRAPFALLBACKTSP.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {


                                    var data = row.BOOTSTRAPFALLBACKMSISDN.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {
                                    var data = row.BOOTSTRAPFALLBACKSUBSCRIPTIONSTATUS.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.DATEOFCHANGEOVERTOCOMMERCIALPLAN.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.CARDENDDATE.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.COMMERCIALPRIMARYIMSI.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.COMMERCIALPRIMARYTSP.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.COMMERCIALPRIMARYMSISDN.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {
                                    var data = row.COMMERCIALPRIMARYSUBSCRIPTIONSTATUS.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.LASTSRNUMBER.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.LASTSRACTION.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {
                                    var data = row.LASTSRDATE.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            }
                        ],
                        "orderClasses": false,
                        responsive: true,
                        dom: 'lBfrtip',


                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }
                else {
                    table = $('#Alluploadeddata').DataTable({
                        "order": [[1, "desc"]],
                        data: parsedData,
                        stateSave: true,
                        "bDestroy": true,
                        "iDisplayLength": 10,
                        "aLengthMenu": [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                        columns: [
                            //{
                            //    render: function (data, type, row) {
                            //        return row.SRNO;
                            //    }

                            //},
                            {
                                render: function (data, type, row) {
                                    var data = row.SIMNO.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='orig" + cls_original + "'>" + data[0] + "</div><div class='mismatchrecordrow" + cls + "'><input type='checkbox' class='table2_simno'/> &nbsp;" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.CARDSTATE.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.CARDSTATUS.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.CUSTOMERNAME.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },

                            {
                                render: function (data, type, row) {
                                    var data = row.ACCOUNTNO.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.ORDERNO.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }
                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.PRODUCT.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.PROJECT.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.SMSUSAGE.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.DATAUSAGE.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.IMEI.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.BOOTSTRAPPRIMARYIMSI.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.BOOTSTRAPPRIMARYTSP.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.BOOTSTRAPPRIMARYMSISDN.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {
                                    var data = row.BOOTSTRAPPRIMARYSUBSCRIPTIONSTATUS.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";


                                }
                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.BOOTSTRAPFALLBACKIMSI.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.BOOTSTRAPFALLBACKTSP.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {


                                    var data = row.BOOTSTRAPFALLBACKMSISDN.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {
                                    var data = row.BOOTSTRAPFALLBACKSUBSCRIPTIONSTATUS.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.DATEOFCHANGEOVERTOCOMMERCIALPLAN.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.CARDENDDATE.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.COMMERCIALPRIMARYIMSI.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.COMMERCIALPRIMARYTSP.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.COMMERCIALPRIMARYMSISDN.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {
                                    var data = row.COMMERCIALPRIMARYSUBSCRIPTIONSTATUS.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.LASTSRNUMBER.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    var data = row.LASTSRACTION.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            },
                            {
                                render: function (data, type, row) {
                                    var data = row.LASTSRDATE.split('~');
                                    var cls = "", cls_original = "";
                                    if (data[0] != data[1]) {
                                        cls = "mismatch"
                                        cls_original = "_actual"
                                    }

                                    return "<div class='" + cls_original + "'>" + data[0] + "</div><div class='" + cls + "'>" + data[1] + "</div>";
                                }

                            }
                        ],
                        "orderClasses": false,
                        responsive: true,
                        dom: 'lBfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }

            }
            $("#table_card").show();
        },
        error: function (response) {
            swal({
                title: "oops!",
                text: response.responseText,
                icon: "warning",
            });
        },
        complete: function () {
            NProgress.done();
            hideloader();
        },
    });
}

$("#Processbtn").click(function (e) {
    e.stopImmediatePropagation();
    e.preventDefault();
    if ($(this).attr('btnfor') == "mismatchprocess") {
        var data = [];
        $('#Alluploadeddata tbody tr').each(function (e) {
            var p = $(this);
            if (p.find(".table2_simno").prop('checked')) {
                var DataSheet = {
                    SIMID_OF_TL: $.trim(p.find('.mismatchrecordrow').text())
                }
                data.push(DataSheet);
            }

        });
        var data2 = JSON.stringify({ SIMNo: data });
        $.ajax({
            type: "POST",
            async: true,
            url: "/Admin/Dashboard.aspx/method_processdataintomaintable",
            data: data2,
            beforeSend: function () {
                NProgress.start();
                loader();
            },
            contentType: "application/json",
            dataType: "json",
            success: function (response) {
                if (response.d != "") {
                    swal({
                        title: "Yehh!",
                        text: "Data processed successfully.",
                        icon: "success",
                    });
                    getcountsofprocesseddata();
                    getmismatchdata();
                }

            },
             
            complete: function () {
                NProgress.done();
                hideloader();
            },
        });
    }
    processdata();
    getmismatchdata();
});


$("#Processbtnallnewrecordbtn").click(function (e) {
    e.stopImmediatePropagation();
    e.preventDefault();

    if ($(this).attr('btnfor') =='newentry') {
        InsertNewData();
    } else {
    $(".table2_simno").attr('checked', 'checked');
    $.ajax({
        type: "POST",
        async: true,
        url: "/Admin/Dashboard.aspx/method_processdataAll",
        data: {},
        beforeSend: function () {
            NProgress.start();
            loader();
        },
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            if (response.d != "") {
                swal({
                    title: "Yehh!",
                    text: "Data processed successfully.",
                    icon: "success",
                });
                getcountsofprocesseddata();
                getmismatchdata();
            }

        },
        error: function (response) {
            swal({
                title: "oops!",
                text: response.responseText,
                icon: "warning",
            });
        },
        complete: function () {
            NProgress.done();
            hideloader();
        },
    });
    }
    processdata();
    getmismatchdata();
});



$("#rejectselectedbtn").click(function (e) {
    e.stopImmediatePropagation();
    e.preventDefault();
    var data = [];
    $('#Alluploadeddata tbody tr').each(function (e) {
        var p = $(this);
        if (p.find(".table2_simno").prop('checked')) {
            var DataSheet = {
                SIMID_OF_TL: $.trim(p.find('.mismatchrecordrow').text())
            }
            data.push(DataSheet);
        }

    });
    var data2 = JSON.stringify({ SIMNo: data });
    $.ajax({
        type: "POST",
        async: true,
        url: "/Admin/Dashboard.aspx/method_rejectprocessdata",
        data: data2,
        beforeSend: function () {
            NProgress.start();
            loader();
        },
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            if (response.d != "") {
                swal({
                    title: "Yehh!",
                    text: "Data Rejected successfully.",
                    icon: "success",
                });
                getcountsofprocesseddata();
                getmismatchdata();
            }

        },
        error: function (response) {
            swal({
                title: "oops!",
                text: response.responseText,
                icon: "warning",
            });
        },
        complete: function () {
            NProgress.done();
            hideloader();
        },
    });
    processdata();
    getmismatchdata();

});


$("#viewalluploaddata").click(function (e) {
    e.stopImmediatePropagation();
    e.preventDefault();
    getallsimdata();
});


function getallsimdata() {
    $("#cardtitle").text("All SIM Records");
    $.ajax({
        type: "POST",
        async: true,
        url: "/Admin/Dashboard.aspx/method_getallsimddata",
        data: {},
        beforeSend: function () {
            NProgress.start();
            loader();
        },
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            if (response.d != "") {
                var parsedData = JSON.parse(response.d);

                if ($.fn.dataTable.isDataTable('#Alluploadeddata')) {
                    table = $('#Alluploadeddata').DataTable({
                        "order": [[1, "desc"]],
                        data: parsedData,
                        stateSave: true,
                        "bDestroy": true,
                        "iDisplayLength": 10,
                        "aLengthMenu": [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                        columns: [
                            {
                                render: function (data, type, row) {
                                    return row.SIMNO;
                                }
                            },
                            {
                                render: function (data, type, row) {
                                    return row.CARDSTATE;
                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.CARDSTATUS;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.CUSTOMERNAME;


                                }

                            },

                            {
                                render: function (data, type, row) {
                                    return row.ACCOUNTNO;


                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.ORDERNO;



                                }
                            },
                            {
                                render: function (data, type, row) {

                                    return row.PRODUCT;



                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.PROJECT;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.SMSUSAGE;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.DATAUSAGE;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.IMEI;


                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPPRIMARYIMSI;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPPRIMARYTSP;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPPRIMARYMSISDN;

                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.BOOTSTRAPPRIMARYSUBSCRIPTIONSTATUS;





                                }
                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPFALLBACKIMSI;

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPFALLBACKTSP;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPFALLBACKMSISDN;

                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.BOOTSTRAPFALLBACKSUBSCRIPTIONSTATUS;

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.DATEOFCHANGEOVERTOCOMMERCIALPLAN;


                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.CARDENDDATE;



                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.COMMERCIALPRIMARYIMSI;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.COMMERCIALPRIMARYTSP;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.COMMERCIALPRIMARYMSISDN;

                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.COMMERCIALPRIMARYSUBSCRIPTIONSTATUS;




                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.LASTSRNUMBER;

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.LASTSRACTION;



                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.LASTSRDATE;

                                }

                            }
                        ],
                        "orderClasses": false,
                        responsive: true,
                        dom: 'lBfrtip',


                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }
                else {
                    table = $('#Alluploadeddata').DataTable({
                        "order": [[1, "desc"]],
                        data: parsedData,
                        stateSave: true,
                        "bDestroy": true,
                        "iDisplayLength": 10,
                        "aLengthMenu": [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                        columns: [
                            {
                                render: function (data, type, row) {
                                    return row.SIMNO;
                                }
                            },
                            {
                                render: function (data, type, row) {
                                    return row.CARDSTATE;
                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.CARDSTATUS;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.CUSTOMERNAME;


                                }

                            },

                            {
                                render: function (data, type, row) {
                                    return row.ACCOUNTNO;


                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.ORDERNO;



                                }
                            },
                            {
                                render: function (data, type, row) {

                                    return row.PRODUCT;



                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.PROJECT;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.SMSUSAGE;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.DATAUSAGE;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.IMEI;


                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPPRIMARYIMSI;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPPRIMARYTSP;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPPRIMARYMSISDN;

                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.BOOTSTRAPPRIMARYSUBSCRIPTIONSTATUS;





                                }
                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPFALLBACKIMSI;

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPFALLBACKTSP;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.BOOTSTRAPFALLBACKMSISDN;

                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.BOOTSTRAPFALLBACKSUBSCRIPTIONSTATUS;

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.DATEOFCHANGEOVERTOCOMMERCIALPLAN;


                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.CARDENDDATE;



                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.COMMERCIALPRIMARYIMSI;
                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.COMMERCIALPRIMARYTSP;



                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.COMMERCIALPRIMARYMSISDN;

                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.COMMERCIALPRIMARYSUBSCRIPTIONSTATUS;




                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.LASTSRNUMBER;

                                }

                            },
                            {
                                render: function (data, type, row) {

                                    return row.LASTSRACTION;



                                }

                            },
                            {
                                render: function (data, type, row) {
                                    return row.LASTSRDATE;

                                }

                            }
                        ],
                        "orderClasses": false,
                        responsive: true,
                        dom: 'lBfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }


                $("#table_card").show();

            }
        },
        error: function (response) {
            swal({
                title: "oops!",
                text: response.responseText,
                icon: "warning",
            });
        },
        complete: function () {
            NProgress.done();
            hideloader();
        },
    });
}

function imageCheck(event, current) {
    var reader = new FileReader();
    var readerBase64 = new FileReader();
    var image = event.target.files[0];
    var realMimeHeader = '';
    var base64 = '';
    if (image == undefined) {
        $(current).removeAttr('attachment');
        $(current).removeAttr('attachment_type');
        return;
    }
    reader.onloadend = function () {
        realMimeHeader = getRealMimeType(reader);
        readerBase64.readAsDataURL(image);
    };
    reader.readAsArrayBuffer(image);
    readerBase64.onloadend = function () {
        base64 = this.result;
        $(current).attr('attachment', base64);
        $(current).attr('attachment_type', realMimeHeader);
    };

}
function getRealMimeType(reader) {
    var arr = (new Uint8Array(reader.result)).subarray(0, 4);
    var header = '';
    for (var i = 0; i < arr.length; i++) {
        header += arr[i].toString(16);
    }
    return header;
}

var link = window.location.origin;
function uploadFile() {
    try {
        if ($('#formFile').attr('attachment') != '' && $('#formFile').attr('attachment') != undefined) {
            var url = link + "/Admin/Dashboard.aspx//uploadExcelFile";
            var base64 = $('#formFile').attr('attachment');
            var Header = $('#formFile').attr('attachment_type');
            var data = JSON.stringify({ Base64: base64, Header: Header });
            var Result = doAjax(url, data);
            if (Result != '') {
                swal('Upload Alert', Result, 'error');
                if (Result == 'Invalid User') {
                    Logout();
                }
                $('#formFile').val('');
                $('#formFile').removeAttr('attachment');
                $('#formFile').removeAttr('attachment_type');
            }
            else {
                swal('Data Alert', 'Data Uploaded Successfully', 'success');
                $('#formFile').val('');
                $('#formFile').removeAttr('attachment');
                $('#formFile').removeAttr('attachment_type');
                getPrcessData();
            }

        }
        else {
            swal('Upload Alert', 'Please attach file first', 'error');
        }


    } catch (error) {
        console.log(error);
    }
}

function ProcessData() {
    var url = link + "/Admin/Dashboard.aspx/ProcessData";
    var data = {};
    var Result = doAjax(url, data);
    getPrcessData();
}

function getPrcessData() {
    var url = link + "/Admin/Dashboard.aspx/getPrcessData";
    var data = {};
    var Result = doAjax(url, data);
    if (Result != '') {
        var Data1 = JSON.parse(Result).table;
        var Data2 = JSON.parse(Result).table1;
        var Data3 = JSON.parse(Result).table2;
        var Data4 = JSON.parse(Result).table3;
        var Data5 = JSON.parse(Result).table4;
        var Data6 = JSON.parse(Result).table5;
        $('#processdata__btn1').attr('disabled', true);
        if (Data1.length > 0) {
            if (Data1[0].status == 'Y') {
                $('#processdata__btn1').attr('disabled', false);
            }
        }

        $('#Total_sim_count').html(Data2[0].simcount);
        $('#newrecorddisplay').html(Data3[0].newsimrecordscount);
        $('#mismatchrecorddisplay').html(Data4[0].mismatchedsimrecordscount);
        $('#samerecorddisplay').html(Data5[0].matchedsimrecordscount);
        $('#divUploaded').hide();
        if (Data6.length > 0) {
            $('#divUploaded').show();
            $('#LastUploadedDate').html(Data6[0].createddate)
        }
    }
}


function getNewRecords() {
    try {
        $("#cardtitle").text("New Records");
        var url = link + "/Admin/Dashboard.aspx/method_getuploadeddata";
        var data = JSON.stringify({ foruse: 'NewRecord' });
        var Result = doAjax(url, data);
        $("#Processbtn").hide();
        $("#Processbtnallnewrecordbtn").show();
        $("#rejectselectedbtn").show();
        $("#Processbtnallnewrecordbtn").removeAttr("disabled");
        $("#Processbtnallnewrecordbtn").attr('btnfor','newentry')
        $("#rejectselectedbtn").removeAttr("disabled");
        if (Result != "") {
            var parsedData = JSON.parse(Result).table;
             if ($.fn.dataTable.isDataTable('#Alluploadeddata')) {
                table = $('#Alluploadeddata').DataTable({
                     data: parsedData,
                     stateSave: true,
                    "bDestroy": true,
                    "iDisplayLength": 10,
                    "aLengthMenu": [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                    columns: [
                        {
                            render: function (data, type, row) {
                                return row.SIMNO;
                            }
                        },
                        {
                            render: function (data, type, row) {
                                return row.CARDSTATE;
                            }

                        },
                        {
                            render: function (data, type, row) {
                                return row.CARDSTATUS;
                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.CUSTOMERNAME;


                            }

                        },

                        {
                            render: function (data, type, row) {
                                return row.ACCOUNTNO;


                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.ORDERNO;



                            }
                        },
                        {
                            render: function (data, type, row) {

                                return row.PRODUCT;



                            }

                        },
                        {
                            render: function (data, type, row) {
                                return row.PROJECT;
                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.SMSUSAGE;



                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.DATAUSAGE;



                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.IMEI;


                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.BOOTSTRAPPRIMARYIMSI;
                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.BOOTSTRAPPRIMARYTSP;



                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.BOOTSTRAPPRIMARYMSISDN;

                            }

                        },
                        {
                            render: function (data, type, row) {
                                return row.BOOTSTRAPPRIMARYSUBSCRIPTIONSTATUS;





                            }
                        },
                        {
                            render: function (data, type, row) {

                                return row.BOOTSTRAPFALLBACKIMSI;

                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.BOOTSTRAPFALLBACKTSP;



                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.BOOTSTRAPFALLBACKMSISDN;

                            }

                        },
                        {
                            render: function (data, type, row) {
                                return row.BOOTSTRAPFALLBACKSUBSCRIPTIONSTATUS;

                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.DATEOFCHANGEOVERTOCOMMERCIALPLAN;


                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.CARDENDDATE;



                            }

                        },
                        {
                            render: function (data, type, row) {
                                return row.COMMERCIALPRIMARYIMSI;
                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.COMMERCIALPRIMARYTSP;



                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.COMMERCIALPRIMARYMSISDN;

                            }

                        },
                        {
                            render: function (data, type, row) {
                                return row.COMMERCIALPRIMARYSUBSCRIPTIONSTATUS;




                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.LASTSRNUMBER;

                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.LASTSRACTION;



                            }

                        },
                        {
                            render: function (data, type, row) {
                                return row.LASTSRDATE;

                            }

                        }
                    ],
                    "orderClasses": false,
                    responsive: true,
                    dom: 'lBfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });
            }
            else {
                table = $('#Alluploadeddata').DataTable({
                    data: parsedData,
                    stateSave: true,
                    "bDestroy": true,
                    "iDisplayLength": 10,
                    "aLengthMenu": [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                    columns: [
                        {
                            render: function (data, type, row) {
                                return row.SIMNO;
                            }
                        },
                        {
                            render: function (data, type, row) {
                                return row.CARDSTATE;
                            }

                        },
                        {
                            render: function (data, type, row) {
                                return row.CARDSTATUS;
                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.CUSTOMERNAME;


                            }

                        },

                        {
                            render: function (data, type, row) {
                                return row.ACCOUNTNO;


                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.ORDERNO;



                            }
                        },
                        {
                            render: function (data, type, row) {

                                return row.PRODUCT;



                            }

                        },
                        {
                            render: function (data, type, row) {
                                return row.PROJECT;
                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.SMSUSAGE;



                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.DATAUSAGE;



                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.IMEI;


                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.BOOTSTRAPPRIMARYIMSI;
                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.BOOTSTRAPPRIMARYTSP;



                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.BOOTSTRAPPRIMARYMSISDN;

                            }

                        },
                        {
                            render: function (data, type, row) {
                                return row.BOOTSTRAPPRIMARYSUBSCRIPTIONSTATUS;





                            }
                        },
                        {
                            render: function (data, type, row) {

                                return row.BOOTSTRAPFALLBACKIMSI;

                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.BOOTSTRAPFALLBACKTSP;



                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.BOOTSTRAPFALLBACKMSISDN;

                            }

                        },
                        {
                            render: function (data, type, row) {
                                return row.BOOTSTRAPFALLBACKSUBSCRIPTIONSTATUS;

                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.DATEOFCHANGEOVERTOCOMMERCIALPLAN;


                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.CARDENDDATE;



                            }

                        },
                        {
                            render: function (data, type, row) {
                                return row.COMMERCIALPRIMARYIMSI;
                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.COMMERCIALPRIMARYTSP;



                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.COMMERCIALPRIMARYMSISDN;

                            }

                        },
                        {
                            render: function (data, type, row) {
                                return row.COMMERCIALPRIMARYSUBSCRIPTIONSTATUS;




                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.LASTSRNUMBER;

                            }

                        },
                        {
                            render: function (data, type, row) {

                                return row.LASTSRACTION;



                            }

                        },
                        {
                            render: function (data, type, row) {
                                return row.LASTSRDATE;

                            }

                        }
                    ],
                    "orderClasses": false,
                    responsive: true,
                    dom: 'lBfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });
            }

        }
        else {

            $('#Alluploadeddata').DataTable().clear().draw();
            $('#Alluploadeddata').dataTable().fnClearTable();

        }
        $("#table_card").show();
       

    } catch (error) {
        console.log(error);
    }
}

function InsertNewData() {
    var url = link + "/Admin/Dashboard.aspx/NewRecordProcessData";
    var data = {};
    var Result = doAjax(url, data);
    if (Result!="") {
        swal('Data Process Alert', Result, 'success');
    }
    getPrcessData();
}





