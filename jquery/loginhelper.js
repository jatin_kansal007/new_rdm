﻿//"warning"
//"error"
//"success"
//"info"


$(function () {
    generate();
});

let captcha;
let alphabets = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";

generate = () => {

    var url = window.location.origin + "/login.aspx/generateCaptcha";
    var data = {}
    try {
        var Result = doAjax(url, data);
        document.getElementById('generated-captcha').value = Result;
    } catch (error) {
        console.log(error);
    }

}

function check() {
    // console.log(status)
    let userValue = document.getElementById("entered-captcha").value;

    if (userValue != captcha) {
        document.getElementById("entered-captcha").value = '';
        swal('Captcha Alert', 'Captcha is not ')
        return;
    }
}

function doAjax(url, params, method) {
    var Result = "";
    $.ajax({
        'async': false,
        url: url,
        type: method || 'POST',
        contentType: "application/json",
        dataType: 'json',
        data: params,
        success: function (data) {
            Result = data.d;

        },
        error: function (jqXHR, exception) {
            if (jqXHR.status === 0) {
                alert('Not connect.\n Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found. [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (exception === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (exception === 'timeout') {
                alert('Time out error.');
            } else if (exception === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error.\n' + jqXHR.responseText);
            }
        }
    });
    return Result;
}

function CheckCredentials() {
    var username = $.trim($("#InputMobile").val());
    var password = $.trim($("#InputPassword").val());
    if (username.length <= 0) {
        swal({
            title: "oops!",
            text: "please enter valid mobile number",
            icon: "error",
        }).then(function () {
            $("#InputMobile").focus();
        });
        return;
    }
    else if (password.length <= 0) {
        swal({
            title: "oops!",
            text: "please enter your correct password",
            icon: "error",
        }).then(function () {
            $("#InputPassword").focus();
            return;
        });
    }
    var Captcha = $.trim($('#entered-captcha').val());
    if (Captcha.length == 0) {
        swal({
            title: "oops!",
            text: "Please enter Captcha",
            icon: "error",
        })
        $("#entered-captcha").focus();
        return;

    }
    var url = window.location.origin + "/login.aspx/methodgetloginusersdata";
    var data = JSON.stringify({ UserName: username, Password: password, Captcha: Captcha });
    try {
        var Result = doAjax(url, data);
        Result = Result.split('|');
        if (!isNullOrEmpty(Result[1])) {
            swal({
                title: "Success!",
                text: "Login successfully",
                icon: "success",
            }).then(function () {
                window.location = Result[1];
            });
        }
        else {
            swal({
                title: "Login Alert!",
                text: Result[0],
                icon: "error",
            });
            return;
        }

    } catch (error) {
        console.log(error);
    }
}

function isNullOrEmpty(str) {
    var returnValue = false;
    if (!str
        || str == null
        || str === 'null'
        || str === ''
        || str === '{}'
        || str === 'undefined'
        || str.length === 0) {
        returnValue = true;
    }
    return returnValue;
}