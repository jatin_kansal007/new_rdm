﻿
$(document).ready(function () {
    if (performance.navigation.type == 2) {
        location.reload(true);
    }
})

function NumberOnly(e) {
    var event = e || window.event

    var key_code = event.keyCode;
    var oElement = e ? e.target : window.event.srcElement;
    if (!event.shiftKey && !event.ctrlKey && !event.altKey) {
        if ((key_code > 47 && key_code < 58) ||
            (key_code > 95 && key_code < 106)) {

            if (key_code > 95)
                key_code -= (95 - 47);
            oElement.value = oElement.value;
        } else if (key_code == 8) {
            oElement.value = oElement.value;
        } else if (key_code != 9) {
            event.returnValue = false;
        }
    }
}

//// for true or false
function isNullOrEmpty(str) {
    var returnValue = false;
    if (!str
        || str == null
        || str === 'null'
        || str === ''
        || str === '{}'
        || str === 'undefined'
        || str.length === 0) {
        returnValue = true;
    }
    return returnValue;
}

/// for var return empty
function getNullOrEmpty(str) {
    var returnValue = $.trim(str);
    if (!str
        || str == null
        || str === 'null'
        || str === ''
        || str === '{}'
        || str === 'undefined'
        || str.length === 0) {
        returnValue = '';
    }
    return returnValue;
}

function doAjax(url, params, method) {
    var Result = "";
    $.ajax({
        'async': false,
        url: url,
        type: method || 'POST',
        contentType: "application/json",
        dataType: 'json',
        data: params,
        beforeSend: function () {
            NProgress.start();
            loader();
        },
        success: function (data) {
            Result = data.d;
        },
        error: function (jqXHR, exception) {
            if (jqXHR.responseText.indexOf('session') > -1) {
                Logout();
            }
            else {
                if (jqXHR.status === 0) {
                    alert('Not connect.\n Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found. [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (exception === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (exception === 'timeout') {
                    alert('Time out error.');
                } else if (exception === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText);
                }
            }
        },
        complete: function () {
            NProgress.done();
            hideloader();
        },
    });
    return Result;
}

function Logout() {
    var url = link + "/login.aspx/LogOut";
    var data = {};
    var Result = doAjax(url, data);
    window.location.href = Result;
}

//document.onkeydown = function (e) {
//    e.preventDefault();
//    if (location.hostname === "localhost" || location.hostname === "127.0.0.1" || location.hostname === "") {

//    }
//    else {
//        // "I" key
//        if (e.ctrlKey && e.shiftKey && e.keyCode == 73) {
//            disabledEvent(e);
//        }
//        // "J" key
//        if (e.ctrlKey && e.shiftKey && e.keyCode == 74) {
//            disabledEvent(e);
//        }
//        // "S" key + macOS
//        if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
//            disabledEvent(e);
//        }
//        // "U" key
//        if (e.ctrlKey && e.keyCode == 85) {
//            disabledEvent(e);
//        }
//        // "F12" key
//        if (event.keyCode == 123) {
//            disabledEvent(e);
//        }
//    }
//}

//function disabledEvent(e) {
//    if (e.stopPropagation) {
//        e.stopPropagation();
//    } else if (window.event) {
//        window.event.cancelBubble = true;
//    }
//    e.preventDefault();
//    return false;

//}
