﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Distributor/DistributorMain.master" AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="Distributor_Dashboard" %>

<asp:Content ID="Content2" ContentPlaceHolderID="headercss" runat="Server">
    <link href="../css/DistributorStyle.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="pagetitle">
        <h1>Dashboard</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="admin">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </nav>

    </div>
    <!-- End Page Title -->

    <section class="section dashboard">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="box DistributorDetails">
                                    <div class="DistributorAvtarBox">
                                        <img src="../assets/img/man.png" />
                                    </div>
                                    <div class="DistributorDetailsBox">
                                        <h4>DistributorName (RDM001)</h4>
                                        <div class="ContactNumber">9876543210</div>
                                        <div class="CompleteAddress">
                                            Maruthi Illam, 7, Pavalamalli Street,
                                            Poonga Nagar, Tiruvallur - 602 001.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="box totaldevice">
                                            <h5>Total Device</h5>
                                            <div id="totaldevice">
                                                <i style="font-size: 40px; color: #4659f2;" class="bi bi-motherboard"></i>&nbsp;&nbsp;<span id="TotalCount">1230</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="box instockdevice">
                                            <h5>In Stock</h5>
                                            <div id="stock">
                                                <i style="font-size: 40px; color: #4659f2;" class="bi bi-motherboard"></i>&nbsp;&nbsp;<span id="TotalCountstock">230</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div data-bs-toggle="modal" data-bs-target="#basicModal" class="box orderdevice">
                                            <i style="font-size: 45px; color: #4659f2;" class="bi bi-plus-square-fill"></i>
                                            <h5>Purchase Device</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="box">
                                    <div class="form-group">
                                        <label>Upload File</label>
                                        <input class="form-control" type="file" id="formFile">
                                    </div>
                                    <div class="uploadBtn">
                                        <button type="button" id="UploadFile" class="btn btn-primary btn-sm BtnStyle" style="height: 40px;">
                                            Upload
                                        </button>
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="box">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="box"></div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="box"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="card">
                        <div class="table-responsive">
                            <table id="Alluploadeddata" class="table table-borderless datatable">
                                <thead>
                                    <tr>
                                        <%-- <th>#</th>--%>
                                        <th>SrNo</th>
                                        <th>CARDSTATE</th>
                                        <th>CARDSTATUS</th>
                                        <th>CUSTOMERNAME</th>
                                        <th>ACCOUNTNO</th>
                                        <th>ORDERNO</th>
                                        <th>PRODUCT</th>
                                        <th>PROJECT</th>
                                        <th>SMSUSAGE</th>

                                    </tr>
                                </thead>

                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="basicModal" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Qty for order device</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="form-floating mb-3">
                        <input type="number" class="form-control" id="floatingInputQty" placeholder="0">
                        <label for="floatingInputQty">Enter Qty</label>
                    </div>
                    <div class="buttons">
                        <button id="SubmitOrderQty" class="BtnStyle btn btn-primary">Proceed</button>
                    </div>

                </div>

            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JqueryPlaceHolder" runat="Server">
    <script src="../jquery/DistributorHelper.js"></script>
</asp:Content>
