﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Distributor/DistributorMain.master" AutoEventWireup="true" CodeFile="NewDealerCustomer.aspx.cs" Inherits="Distributor_NewDealerCustomer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headercss" runat="Server">
    <link href="../css/DistributorStyle.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="pagetitle">
        <h1>Dashboard</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="admin">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </nav>

    </div>
    <!-- End Page Title -->

    <section class="section dashboard">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card NewDealerRegistration">
                        <div class="newDealer">
                            <div class="Header">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button type="button" id="OpenDownBtn" class="btn btn-sm btn-primary BtnStyle" style="height: 40px;">
                                            Add Dealer
                                        </button>
                                         <button type="button" id="Distributor_submitbtn" class="btn btn-sm btn-success BtnStyle float-right" style="height: 40px;">
                                            Submit
                                        </button>
                                          <button type="button" id="Cancelbtn" class="btn btn-sm  btn-default BtnStyle float-right" style="height: 40px;">
                                            Cancel
                                        </button>
                                       
                                    </div>
                                </div>
                                <div class="FormBody__ close">
                                    <div class="card">
                                        <div class="forms">
                                            <div class="form-header">
                                                <h4>Personal Details
                                                    <span class="angles"><i class="bi bi-caret-down-fill"></i></span>
                                                </h4>
                                            </div>
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-3 mb-3">
                                                        <label for="DealerName" class="form-label">Name</label>
                                                        <input type="text" class="form-control" placeholder="Dealer Name" id="DealerName" />
                                                    </div>
                                                    <div class="col-md-3 mb-3">
                                                        <label for="Mobile" class="form-label">Mobile</label>
                                                        <input type="number" class="form-control" placeholder="+91 0000000000" id="Mobile">
                                                    </div>
                                                    <div class="col-md-3 mb-3">
                                                        <label for="AlternateMobile" class="form-label">Alternate Mobile</label>
                                                        <input type="Number" class="form-control" placeholder="+91 0000000000" id="AlternateMobile">
                                                    </div>
                                                    <div class="col-md-3 mb-3">
                                                        <label for="EmailId" class="form-label">Email</label>
                                                        <input type="email" placeholder="@gmail.com, @yahoo.com" class="form-control" id="EmailId">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="card">
                                        <div class="forms">
                                            <div class="form-header">
                                                <h4>Company Details
                                                    <span class="angles"><i class="bi bi-caret-down-fill"></i></span>
                                                </h4>
                                            </div>
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-4 mb-3">
                                                        <label for="CompanyName" class="form-label">Company Name</label>
                                                        <input type="text" class="form-control" placeholder="Dealer Name" id="CompanyName" />
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="Com_Mobile" class="form-label">Mobile</label>
                                                        <input type="number" class="form-control" placeholder="+91 0000000000" id="Com_Mobile">
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="Com_AlternateMobile" class="form-label">Alternate Mobile</label>
                                                        <input type="Number" class="form-control" placeholder="+91 0000000000" id="Com_AlternateMobile">
                                                    </div>
                                                    <div class="col-md-5 mb-3">
                                                        <label for="Com_EmailId" class="form-label">Email</label>
                                                        <input type="email" placeholder="@gmail.com, @yahoo.com" class="form-control" id="Com_EmailId">
                                                    </div>
                                                    <div class="col-md-7 mb-3">
                                                        <label for="CompleteAddress" class="form-label">Complete Address</label>
                                                        <input type="text" class="form-control" id="CompleteAddress">
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="City" class="form-label">City</label>
                                                        <input type="text" class="form-control" id="City">
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="State" class="form-label">State</label>
                                                        <input type="text" class="form-control" id="State">
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="Pincode" class="form-label">Pincode</label>
                                                        <input type="text" class="form-control" id="Pincode">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="card">
                                        <div class="forms">
                                            <div class="form-header">
                                                <h4>Payment Details
                                                    <span class="angles"><i class="bi bi-caret-down-fill"></i></span>
                                                </h4>
                                            </div>
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-4 mb-3">
                                                        <label for="PerUnitPrice" class="form-label">Per Unit Price</label>
                                                        <input type="number" class="form-control" placeholder="0" id="PerUnitPrice" />
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="CreditLimit" class="form-label">Credit Limit</label>
                                                        <input type="number" class="form-control" placeholder="0" id="CreditLimit">
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="ActivationCharge" class="form-label">Activation Charge</label>
                                                        <input type="Number" class="form-control" placeholder="0" id="ActivationCharge">
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="PDCharge" class="form-label">PD Charge</label>
                                                        <input type="text" class="form-control" placeholder="0" id="PDCharge">
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="SuspendedCharge" class="form-label">Suspended Charge</label>
                                                        <input type="text" class="form-control" placeholder="0" id="SuspendedCharge">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="card">
                            <div class="table-responsive">
                                <table id="Alluploadeddata" class="table table-borderless datatable">
                                    <thead>
                                        <tr>
                                            <th>Company</th>
                                            <th>Details</th>
                                            <th>Address</th>
                                            <th>Total Device</th>
                                            <th>Dealer Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Company name</td>
                                            <td>
                                                <h6><strong>Dealer Name (RDM001)</strong></h6>
                                                <div class="ContactNumber">9876543210,9876543210</div>
                                            </td>
                                            <td>
                                                <div class="DistributorDetailsBox">
                                                    <div class="CompleteAddress">
                                                        Maruthi Illam, 7, Pavalamalli Street,
                                            Poonga Nagar, Tiruvallur - 602 001.
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="values">200</div>
                                            </td>
                                            <td>
                                                <div class="values">1200</div>
                                            </td>


                                        </tr>
                                        <tr>
                                            <td>Company name</td>
                                            <td>
                                                <h6><strong>Dealer Name (RDM001)</strong></h6>
                                                <div class="ContactNumber">9876543210,9876543210</div>
                                            </td>
                                            <td>
                                                <div class="DistributorDetailsBox">
                                                    <div class="CompleteAddress">
                                                        Maruthi Illam, 7, Pavalamalli Street,
                                            Poonga Nagar, Tiruvallur - 602 001.
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="values">200</div>
                                            </td>
                                            <td>
                                                <div class="values">1200</div>
                                            </td>


                                        </tr>
                                        <tr>
                                            <td>Company name</td>
                                            <td>
                                                <h6><strong>Dealer Name (RDM001)</strong></h6>
                                                <div class="ContactNumber">9876543210,9876543210</div>
                                            </td>
                                            <td>
                                                <div class="DistributorDetailsBox">
                                                    <div class="CompleteAddress">
                                                        Maruthi Illam, 7, Pavalamalli Street,
                                                        Poonga Nagar, Tiruvallur - 602 001.
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="values">200</div>
                                            </td>
                                            <td>
                                                <div class="values">1200</div>
                                            </td>


                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </section>
</asp:Content> 
<asp:Content ID="Content3" ContentPlaceHolderID="JqueryPlaceHolder" runat="Server">
    <script src="../jquery/AddNewDealerHelper.js"></script>
</asp:Content>