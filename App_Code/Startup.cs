﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(rdmenterprises.Startup))]
namespace rdmenterprises
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
