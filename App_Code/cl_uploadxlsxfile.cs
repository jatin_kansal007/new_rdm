﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for cl_uploadxlsxfile
/// </summary>
public class cl_uploadxlsxfile
{
    public cl_uploadxlsxfile()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    DataSet ds = new DataSet();

    public int Type { get; set; }
    public string usefor { get; set; }
    public string User_ID { get; set; }
    public string Original_File_Name { get; set; }
    public string XMLData { get; set; }

    public string SIMNo_TL { get; set; }
    private string str;


    public DataSet uploadExcelData()
    {
        str = "EXEC proc_rdm_admin @for='" + usefor + "',@sensorise_sim_details_Xml='" + XMLData
            + "',@ORIGINAL_FILE_NAME='" + Original_File_Name + "',@USER_ID='" + User_ID 
            + "',@SIMNo_TL='" + SIMNo_TL + "'";
        dal d = dal.GetInstance();
        ds = d.GetDataSet(str);
        if (ds != null)
        {
            return ds;
        }
        else
        {
            return null;
        }
    }
}

public class CL_EXCEL_DATA_UPLOAD_LIST2
{

    public string SrNo { get; set; }
    public string SIMNo { get; set; }
    public string CardState { get; set; }
    public string CardStatus { get; set; }
    public string CustomerName { get; set; }
    public string AccountNo { get; set; }
    public string OrderNo { get; set; }
    public string Product { get; set; }
    public string Project { get; set; }
    public string SMSUsage { get; set; }
    public string Datausage { get; set; }
    public string IMEI { get; set; }
    public string BootstrapPrimaryIMSI { get; set; }
    public string BootstrapPrimaryTSP { get; set; }
    public string BootstrapPrimaryMSISDN { get; set; }
    public string BootstrapPrimarySubscriptionStatus { get; set; }
    public string BootstrapPrimaryActivationDate { get; set; }
    public string BootstrapFallBackIMSI { get; set; }
    public string BootstrapFallBackTSP { get; set; }
    public string BootstrapFallBackMSISDN { get; set; }
    public string BootstrapFallBackSubscriptionStatus { get; set; }
    public string BootstrapFallBackActivationDate { get; set; }
    public string DateofChangeovertoCommercialPlan { get; set; }
    public string CardEndDate { get; set; }
    public string CommercialPrimaryIMSI { get; set; }
    public string CommercialPrimaryTSP { get; set; }
    public string CommercialPrimaryMSISDN { get; set; }
    public string CommercialPrimarySubscriptionStatus { get; set; }
    public string CommercialFallbackIMSI { get; set; }
    public string CommercialFallbackTSP { get; set; }
    public string CommercialFallbackMSISDN { get; set; }
    public string CommercialFallbackSubscriptionStatus { get; set; }
    public string CommercialAlternateIMSI { get; set; }
    public string CommercialAlternateTSP { get; set; }
    public string CommercialAlternateMSISDN { get; set; }
    public string CommercialAlternateSubscriptionStatus { get; set; }
    public string LastSRNumber { get; set; }
    public string LastSRAction { get; set; }
    public string LastSRProduct { get; set; }
    public string LastSRdate { get; set; }


}

public class cl_simno_list
{
    public string SIMID_OF_TL { get; set; }
}