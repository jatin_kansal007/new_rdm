﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for cl_AddNewDistributor
/// </summary>
public class cl_AddNewDistributor
{
    public cl_AddNewDistributor()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string UserId { get; set; }
    public string ExcelSheet { get; set; }
    public string Mode { get; set; }
    public string ActionUserId { get; set; }
    public string Name { get; set; }
    public string Email { get; set; }
    public string UserType { get; set; }
    public string Mobile { get; set; }
    public string CompanyName { get; set; }
    public string CompanyCity { get; set; }
    public string CompanyMobile { get; set; }
    public string CompanyGstNo { get; set; }
    public string PricePerUnit { get; set; }
    public string CompanyEmail { get; set; }
    public string CompanyAltMobile { get; set; }
    public string CompanyPincode { get; set; }
    public string CompanyState { get; set; }
    public string CompanyAddress { get; set; }

    string m_sqlString = string.Empty;
    public DataSet fnSaveDistributorData()
    {
        m_sqlString = "EXEC Pro_RDM_AdminDistributor @UserId='" + UserId
            + "',@ActionUserId='" + ActionUserId + "',@Mode='" + Mode + "',@Name='" + Name
            + "',@Email='" + Email + "',@UserType='" + UserType + "',@Mobile='" + Mobile
            + "',@CompanyName='" + CompanyName + "',@CompanyCity='" + CompanyCity
            + "',@CompanyMobile='" + CompanyMobile + "',@CompanyGstNo='" + CompanyGstNo + "',@perunitprice='" + PricePerUnit
            + "',@CompanyEmail='" + CompanyEmail + "',@CompanyAltMobile='" + CompanyAltMobile + "',@CompanyPincode='" + CompanyPincode
            + "',@CompanyState='" + CompanyState + "',@CompanyAddress='" + CompanyAddress + "'";
        dal d = dal.GetInstance();
        DataSet ds = d.GetDataSet(m_sqlString);
        if (ds != null && ds.Tables.Count > 0)
        {
            return ds;
        }
        else
        {
            return null;
        }
    }
    public DataSet fngetDistributorList()
    {
        m_sqlString = "EXEC Pro_RDM_AdminDistributor @UserId='" + UserId
            + "',@ActionUserId='" + ActionUserId + "',@Mode='" + Mode + "'";
        dal d = dal.GetInstance();
        DataSet ds = d.GetDataSet(m_sqlString);
        if (ds != null && ds.Tables.Count > 0)
        {
            return ds;
        }
        else
        {
            return null;
        }
    }
}