﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for cl_upload_files
/// </summary>
public class cl_upload_files
{
        public cl_upload_files(string SrNo, string SIMNo, string CardState, string CardStatus, string CustomerName,
        string AccountNo, string OrderNo, string Product, string Project, string SMSUsage, string Datausage
        , string IMEI, string BootstrapPrimaryIMSI, string BootstrapPrimaryTSP, string BootstrapPrimaryMSISDN
        , string BootstrapPrimarySubscriptionStatus, string BootstrapPrimaryActivationDate, string BootstrapFallBackIMSI
        , string BootstrapFallBackTSP, string BootstrapFallBackMSISDN, string BootstrapFallBackSubscriptionStatus
        , string BootstrapFallBackActivationDate, string DateofChangeovertoCommercialPlan, string CardEndDate
        , string CommercialPrimaryIMSI, string CommercialPrimaryTSP, string CommercialPrimaryMSISDN
        , string CommercialPrimarySubscriptionStatus, string CommercialFallbackIMSI, string CommercialFallbackTSP
        , string CommercialFallbackMSISDN, string CommercialFallbackSubscriptionStatus, string CommercialAlternateIMSI
        , string CommercialAlternateTSP, string CommercialAlternateMSISDN, string CommercialAlternateSubscriptionStatus
        , string LastSRNumber, string LastSRAction, string LastSRProduct, string LastSRdate)
        {
        string str = "EXEC proc_rdm_admin @for='FILE_UPLOAD_IN_TL',@SrNo='" + SrNo+"',@SIMNo = '"+SIMNo
            +"',@CardState = '"+CardState+"',@CardStatus = '"+CardStatus+"',@CustomerName = '"+CustomerName+"',@AccountNo = '"+AccountNo
            +"',@OrderNo = '"+OrderNo+"',@Product = '"+Product+"',@Project = '"+Project+"',@SMSUsage = '"+SMSUsage+"',@Datausage = '"+Datausage
            +"',@IMEI = '"+IMEI+"',@BootstrapPrimaryIMSI = '"+BootstrapPrimaryIMSI+"',@BootstrapPrimaryTSP = '"+BootstrapPrimaryTSP
            +"',@BootstrapPrimaryMSISDN = '"+BootstrapPrimaryMSISDN+"',@BootstrapPrimarySubscriptionStatus = '"+BootstrapPrimarySubscriptionStatus
            +"',@BootstrapPrimaryActivationDate = '"+BootstrapPrimaryActivationDate+"',@BootstrapFallBackIMSI = '"+BootstrapFallBackIMSI
            +"',@BootstrapFallBackTSP = '"+BootstrapFallBackTSP+"',@BootstrapFallBackMSISDN = '"+BootstrapFallBackMSISDN+"',@BootstrapFallBackSubscriptionStatus = '"+BootstrapFallBackSubscriptionStatus
            +"',@BootstrapFallBackActivationDate = '"+BootstrapFallBackActivationDate+"',@DateofChangeovertoCommercialPlan = '"+DateofChangeovertoCommercialPlan
            +"',@CardEndDate = '"+CardEndDate+"',@CommercialPrimaryIMSI = '"+CommercialPrimaryIMSI+"',@CommercialPrimaryTSP = '"+CommercialPrimaryTSP+"',@CommercialPrimaryMSISDN = '"+CommercialPrimaryMSISDN
            +"',@CommercialPrimarySubscriptionStatus = '"+CommercialPrimarySubscriptionStatus+"',@CommercialFallbackIMSI = '"+CommercialFallbackIMSI+"',@CommercialFallbackTSP = '"+CommercialFallbackTSP
            +"',@CommercialFallbackMSISDN = '"+CommercialFallbackMSISDN+"',@CommercialFallbackSubscriptionStatus = '"+CommercialFallbackSubscriptionStatus+"',@CommercialAlternateIMSI = '"+CommercialAlternateIMSI
            +"',@CommercialAlternateTSP = '"+CommercialAlternateTSP+"',@CommercialAlternateMSISDN = '"+CommercialAlternateMSISDN+"',@CommercialAlternateSubscriptionStatus = '"+CommercialAlternateSubscriptionStatus
            +"',@LastSRNumber = '"+LastSRNumber+"',@LastSRAction = '"+LastSRAction+"',@LastSRProduct = '"+LastSRProduct+"',@LastSRdate = '"+LastSRdate+"'";
        dal d = dal.GetInstance();
        DataSet ds = d.GetDataSet(str);


    }
    DataSet ds = new DataSet();

    public int Type { get; set; }
    public string usefor { get; set; }
    public string User_ID { get; set; }
    public string Original_File_Name { get; set; }
    public string XMLData { get; set; }
    private string str;



    public DataSet uploadExcelData()
    {
        str = "EXEC proc_rdm_admin @for='" + usefor + "',@sensorise_sim_details_Xml='" + XMLData
            + "',@ORIGINAL_FILE_NAME='" + Original_File_Name + "',@USER_ID='" + User_ID + "'";
        dal d = dal.GetInstance();
        ds = d.GetDataSet(str);
        if (ds != null)
        {
            return ds;
        }
        else
        {
            return null;
        }
    }


}

public class CL_EXCEL_DATA_UPLOAD_LIST
{
    
    public string SrNo { get; set; }
    public string SIMNo { get; set; }
    public string CardState { get; set; }
    public string CardStatus { get; set; }
    public string CustomerName { get; set; }
    public string AccountNo { get; set; }
    public string OrderNo { get; set; }
    public string Product { get; set; }
    public string Project { get; set; }
    public string SMSUsage { get; set; }
    public string Datausage { get; set; }
    public string IMEI { get; set; }
    public string BootstrapPrimaryIMSI { get; set; }
    public string BootstrapPrimaryTSP { get; set; }
    public string BootstrapPrimaryMSISDN { get; set; }
    public string BootstrapPrimarySubscriptionStatus { get; set; }
    public string BootstrapPrimaryActivationDate { get; set; }
    public string BootstrapFallBackIMSI { get; set; }
    public string BootstrapFallBackTSP { get; set; }
    public string BootstrapFallBackMSISDN { get; set; }
    public string BootstrapFallBackSubscriptionStatus { get; set; }
    public string BootstrapFallBackActivationDate { get; set; }
    public string DateofChangeovertoCommercialPlan { get; set; }
    public string CardEndDate { get; set; }
    public string CommercialPrimaryIMSI { get; set; }
    public string CommercialPrimaryTSP { get; set; }
    public string CommercialPrimaryMSISDN { get; set; }
    public string CommercialPrimarySubscriptionStatus { get; set; }
    public string CommercialFallbackIMSI { get; set; }
    public string CommercialFallbackTSP { get; set; }
    public string CommercialFallbackMSISDN { get; set; }
    public string CommercialFallbackSubscriptionStatus { get; set; }
    public string CommercialAlternateIMSI { get; set; }
    public string CommercialAlternateTSP { get; set; }
    public string CommercialAlternateMSISDN { get; set; }
    public string CommercialAlternateSubscriptionStatus { get; set; }
    public string LastSRNumber { get; set; }
    public string LastSRAction { get; set; }
    public string LastSRProduct { get; set; }
    public string LastSRdate { get; set; }
    

}