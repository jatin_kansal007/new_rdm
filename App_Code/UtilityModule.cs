﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UtilityModule
/// </summary>
public class UtilityModule
{
    public UtilityModule()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static bool CheckAdminSession()
    {
        bool exists = false;
        if (HttpContext.Current.Session["UserD"] != null || HttpContext.Current.Request.Cookies["UserD"] != null)
        {
            exists = true;
        }
        return exists;
    }

    public static string getLoginPageUrl()
    {
        string url = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
        if (HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority).ToLower().Contains("localhost"))
        {
            url = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath + "Login.aspx";
        }
        return url;
    }

    public static bool checkUserAuth(string UserName, string Password, string Token)
    {
        bool result = false;
        try
        {
            cl_login_user cl = new cl_login_user();
            DataSet ds = new DataSet();
            cl.username = UserName;
            cl.password = Password;
            cl.Token = Token;
            cl.Mode = "VerifyToken";
            ds = cl.verifyToken();
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                result = true;
            }
        }
        catch (Exception ex)
        {
            new ExceptionLogging(ex);
        }

        return result;
    }

    public static string getParamFromUrl(string Param)
    {
        string ParamResult = "";
        string QueryString = "";
        if (HttpContext.Current.Session["UserD"] != null)
        {
            QueryString = HttpContext.Current.Session["UserD"].ToString();
        }
        else if (HttpContext.Current.Request.Cookies["UserD"] != null)
        {
            QueryString = HttpContext.Current.Request.Cookies["UserD"].Value.ToString();
        }

        string Qstring = PasswordEncryptDecrypt.DecryptString(HttpUtility.UrlDecode(QueryString));
        ParamResult = HttpUtility.ParseQueryString(Qstring).Get(Param).Trim();
        return ParamResult;
    }
    public static bool getUserTokenCheck()
    {
        bool resullt = false;
        try
        {
            string QueryString = "";
            if (HttpContext.Current.Session["UserC"] != null)
            {
                QueryString = HttpContext.Current.Session["UserC"].ToString();
            }
            else if (HttpContext.Current.Request.Cookies["UserC"] != null)
            {
                QueryString = HttpContext.Current.Request.Cookies["UserC"].Value.ToString();
            }
            if (QueryString == "")
            {
                return resullt;
            }
            else
            {
                string Qstring = PasswordEncryptDecrypt.DecryptString(HttpUtility.UrlDecode(QueryString));
                if (Qstring != "")
                {
                    string UserName = HttpUtility.ParseQueryString(Qstring).Get("username").Trim();
                    string Password = HttpUtility.ParseQueryString(Qstring).Get("password").Trim();
                    string Token = HttpUtility.ParseQueryString(Qstring).Get("token").Trim();
                    resullt = checkUserAuth(UserName, Password, Token);
                    return resullt;
                }
            }
        }
        catch (Exception ex)
        {
            new ExceptionLogging(ex);
        }
        return resullt;
    }
    public static bool CheckExcelFileMimeType(string MimeType)
    {
        bool result = false;
        switch (MimeType)
        {
            case "2253204e":
                result = true;
                break;
            case "504b34":
                result = true;
                break;
            case "d0cf11e0":
                result = true;
                break;
            case "53724e6f":
                result = true;
                break;
            default:
                break;
        }
        return result;
    }

    public static string getExcelMimeName(string MimeType)
    {
        string FileType = "";
        switch (MimeType)
        {
            case "2253204e":
                FileType = "csv";
                break;
            case "53724e6f":
                FileType = "csv";
                break;
            case "504b34":
                FileType = "xlsx";
                break;
            case "d0cf11e0":
                FileType = "xls";
                break;
            default:
                break;
        }
        return FileType;
    }


}