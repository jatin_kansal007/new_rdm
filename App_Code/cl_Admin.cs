﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for cl_Admin
/// </summary>
public class cl_Admin
{
    public cl_Admin()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string UserId { get; set; }
    public string ExcelSheet { get; set; }
    public string Mode { get; set; }
    string m_sqlString = string.Empty;
    public DataSet fnAdminUploadExcel()
    {
        m_sqlString = "EXEC proc_rdm_admin @UserId='" + UserId + "',@XmlExcel='" + ExcelSheet
            + "',@Mode='" + Mode + "'";
        dal d = dal.GetInstance();
        DataSet ds = d.GetDataSet(m_sqlString);
        if (ds != null && ds.Tables.Count > 0)
        {
            return ds;
        }
        else
        {
            return null;
        }
    }
}