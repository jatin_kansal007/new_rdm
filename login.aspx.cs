﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.NetworkInformation;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            clearSession();
        }
    }

    private void clearSession()
    {
        HttpContext.Current.Session.Abandon();
        HttpContext.Current.Session.Clear();
        if (HttpContext.Current != null)
        {
            int cookieCount = HttpContext.Current.Request.Cookies.Count;
            for (var i = 0; i < cookieCount; i++)
            {
                var cookie = HttpContext.Current.Request.Cookies[i];
                if (cookie != null)
                {
                    var expiredCookie = new HttpCookie(cookie.Name)
                    {
                        Expires = DateTime.Now.AddDays(-1),
                        Domain = cookie.Domain
                    };
                    HttpContext.Current.Response.Cookies.Add(expiredCookie); // overwrite it
                }
            }
            HttpContext.Current.Request.Cookies.Clear();
        }
        HttpCookie mycookie = new HttpCookie("checkCookie");
        mycookie.Value = "Y";  // Case sensitivity
        mycookie.Expires = DateTime.Now.AddDays(365);
        HttpContext.Current.Response.Cookies.Add(mycookie);

    }


    [WebMethod]
    public static string methodgetloginusersdata(string UserName, string Password,string Captcha)
    {
        string data = "UserName or Password is invaild.";
        try
        {
            if (string.IsNullOrEmpty(UserName))
            {
                data = "Please enter your Username";
                return data;
            }
            else if (string.IsNullOrEmpty(Password))
            {
                data = "Please enter your Password";
                return data;
            }
            else if(HttpContext.Current.Session["captcha"].ToString() != Captcha)
            {
                data = "Invalid Captcha";
                return data;
            }
            cl_login_user cl = new cl_login_user();
            cl.username = UserName;
            cl.password = Password;
            cl.usefor = "USERS_LOGIN_OF_RDM";
            cl.UserIP = (UserIpAndMacAddress.GetAllLocalIPv4(NetworkInterfaceType.Wireless80211))[0].ToString();
            cl.UserMacAddress = UserIpAndMacAddress.GetMacAddress();
            cl.Mode = "CheckUserLogin";
            cl.Platform = "W";
            cl.DeviceName = UserIpAndMacAddress.getDeviceName();
            DataSet ds = new DataSet();
            ds = cl.checkUserCredentials();
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["UserType"].ToString().ToUpper().Trim() == "A")
                {
                    string Url = "username=" + ds.Tables[0].Rows[0]["username"].ToString()
                        + "&password=" + ds.Tables[0].Rows[0]["password"].ToString() + " &token= "
                        + ds.Tables[0].Rows[0]["Token"].ToString();
                    data = "Login Successfully|Admin/Dashboard.aspx?" + HttpUtility.UrlEncode(PasswordEncryptDecrypt.EncryptString(Url));
                    return data;
                }
            }
        }
        catch (Exception ex)
        {
            new ExceptionLogging(ex);
        }
        return data;

    }


    [WebMethod]

    public static  string generateCaptcha()
    {
        var resultString="";
        try
        {
            var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var Charsarr = new char[6];
            var random = new Random();

            for (int i = 0; i < Charsarr.Length; i++)
            {
                Charsarr[i] = characters[random.Next(characters.Length)];
            }

              resultString = new String(Charsarr);

              HttpContext.Current.Session["captcha"] = resultString;


        }
        catch (Exception ex)
        {
            new ExceptionLogging(ex);
        }

        return resultString;
    }

    [WebMethod]

    public static string LogOut()
    {
        return UtilityModule.getLoginPageUrl();
    }
}