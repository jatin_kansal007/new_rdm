﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_AddNewDistributor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckSession();
    }

    private void CheckSession()
    {
        try
        {
            if (!UtilityModule.CheckAdminSession())
            {
                Response.Redirect(UtilityModule.getLoginPageUrl());
            }
        }
        catch (Exception ex)
        {
            new ExceptionLogging(ex);
            Response.Redirect(UtilityModule.getLoginPageUrl());
        }
    }

    [WebMethod]
    public static string SaveDistributorData(string ActionUserId, string Name, string Mobile, string AltMobile, string Email
        , string UserType, string CompanyName, string CompanyMobile, string CompanyAltMobile, string CompanyEmail, string CompanyPincode, string
        CompanyCity, string CompanyState, string CompanyAddress, string CompanyGstNo, string PricePerUnit)
    {
        string Result = "";
        try
        {
            if (UtilityModule.getUserTokenCheck())
            {
                if (string.IsNullOrEmpty(Name))
                {
                    Result = "Please enter Name";
                    return Result;
                }
                else if (string.IsNullOrEmpty(Mobile))
                {
                    Result = "Please enter mobile no";
                    return Result;
                }
                else if (Mobile.Length != 10)
                {
                    Result = "Please enter 10 digits mobile no";
                    return Result;
                }
                else if (string.IsNullOrEmpty(CompanyName))
                {
                    Result = "Please enter company name";
                    return Result;
                }
                else if (string.IsNullOrEmpty(CompanyMobile))
                {
                    Result = "Please enter company mobile no";
                    return Result;
                }

                cl_AddNewDistributor ca = new cl_AddNewDistributor();
                DataSet ds = new DataSet();
                ca.UserId = UtilityModule.getParamFromUrl("userid");
                ca.Mode = "SaveDistributorData";
                ca.ActionUserId = ActionUserId;
                ca.Name = Name;
                ca.Mobile = Mobile;
                ca.Email = Email;
                ca.UserType = UserType;
                ca.CompanyName = CompanyName;
                ca.CompanyMobile = CompanyMobile;
                ca.CompanyAltMobile = CompanyAltMobile;
                ca.CompanyEmail = CompanyEmail;
                ca.CompanyPincode = CompanyPincode;
                ca.CompanyCity = CompanyCity;
                ca.CompanyState = CompanyState;
                ca.CompanyAddress = CompanyAddress;
                ca.CompanyGstNo = CompanyGstNo;
                ca.PricePerUnit = PricePerUnit;
                ds = ca.fnSaveDistributorData();

            }
        }
        catch (Exception ex)
        {
            new ExceptionLogging(ex);
            Result = "Some Error Occured";
            return Result;
        }

        return Result;
    }

    [WebMethod]
    public static string getDistributorList()
    {
        string Result = "";
        try
        {
            if (UtilityModule.getUserTokenCheck())
            {
                cl_AddNewDistributor ca = new cl_AddNewDistributor();
                DataSet ds = new DataSet();
                ca.UserId = UtilityModule.getParamFromUrl("userid");
                ca.Mode = "getDistributorList";
                ds = ca.fngetDistributorList();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    Result = LowercaseJsonSerializer.SerializeObject(ds);
                }
            }
        }
        catch (Exception ex)
        {
            new ExceptionLogging(ex);
            Result = "Some Error Occured";
            return Result;
        }

        return Result;
    }

    [WebMethod]
    public static string getDistributorByUserId(string UserId)
    {
        string Result = "";
        try
        {
            if (UtilityModule.getUserTokenCheck())
            {
                cl_AddNewDistributor ca = new cl_AddNewDistributor();
                DataSet ds = new DataSet();
                ca.UserId = UtilityModule.getParamFromUrl("userid");
                ca.ActionUserId = UserId;
                ca.Mode = "getDistributorByUserId";
                ds = ca.fngetDistributorList();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    Result = LowercaseJsonSerializer.SerializeObject(ds);
                }
            }
        }
        catch (Exception ex)
        {
            new ExceptionLogging(ex);
            Result = "Some Error Occured";
            return Result;
        }

        return Result;
    }
}