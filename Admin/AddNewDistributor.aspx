﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="AddNewDistributor.aspx.cs" Inherits="Admin_AddNewDistributor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="pagetitle">
        <h1>Dashboard</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="admin">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </nav>

    </div>
    <!-- End Page Title -->

    <section class="section dashboard">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card NewDealerDistributorRegistrationForm">
                        <div class="newDealer">
                            <div class="Header">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h5>Add New Distributor</h5>
                                    </div>
                                    <div class="col-sm-12">

                                        <button type="button" id="OpenDownBtn" class="btn btn-sm btn-primary BtnStyle">
                                            Add Distributor
                                        </button>
                                       
                                        <button type="button" id="Distributor_submitbtn" onclick="SaveDistributorData($(this))" class="btn btn-sm btn-success BtnStyle">
                                            Submit
                                        </button>
                                        <button type="button" id="Cancelbtn" class="btn btn-sm  btn-default BtnStyle">
                                            Cancel
                                        </button>
                                         <button type="button" id="ResetFields" onclick="ResetFields()" class="btn btn-sm btn-danger BtnStyle">
                                            Reset
                                        </button>

                                    </div>
                                </div>
                                <div class="FormBody__ close">
                                    <div class="card">
                                        <div class="forms">
                                            <div class="form-header">
                                                <h4>Personal Details
                                                    <span class="angles"><i class="bi bi-caret-down-fill"></i></span>
                                                </h4>
                                            </div>
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-3 mb-3">
                                                        <label for="DealerName" class="form-label">Name</label>
                                                        <input type="text" class="form-control" placeholder="Enter Name" id="DealerName" />
                                                    </div>
                                                    <div class="col-md-3 mb-3">
                                                        <label for="Mobile" class="form-label">Mobile</label>
                                                        <input type="text" onkeydown="NumberOnly(event)" class="form-control" placeholder="Enter 10 digits" maxlength="10" autocomplete="off" id="Mobile">
                                                    </div>
                                                    <div class="col-md-3 mb-3">
                                                        <label for="AlternateMobile" class="form-label">Alternate Mobile</label>
                                                        <input type="text" onkeydown="NumberOnly(event)" class="form-control" placeholder="Enter 10 digits" maxlength="10" autocomplete="off" id="AlternateMobile">
                                                    </div>
                                                    <div class="col-md-3 mb-3">
                                                        <label for="EmailId" class="form-label">Email</label>
                                                        <input type="text" placeholder="Enter Valid Email" class="form-control" id="EmailId">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="card">
                                        <div class="forms">
                                            <div class="form-header">
                                                <h4>Company Details
                                                    <span class="angles"><i class="bi bi-caret-down-fill"></i></span>
                                                </h4>
                                            </div>
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-4 mb-3">
                                                        <label for="UserType" class="form-label">Type</label>
                                                        <select id="UserType" class="form-control" aria-label="Default select example">
                                                            <option selected="" value="DST">Distributor</option>
                                                            <option value="DLR">Dealer</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-md-4 mb-3">
                                                        <label for="CompanyName" class="form-label">Company Name</label>
                                                        <input type="text" class="form-control" placeholder="Enter Company Name" id="CompanyName" />
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="Com_Mobile" class="form-label">Mobile</label>
                                                        <input type="text" onkeydown="NumberOnly(event)" class="form-control" placeholder="Enter 10 digits" maxlength="10" autocomplete="off" id="Com_Mobile">
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="Com_AlternateMobile" class="form-label">Alternate Mobile</label>
                                                        <input type="text" onkeydown="NumberOnly(event)" class="form-control" placeholder="Enter 10 digits" maxlength="10" autocomplete="off" id="Com_AlternateMobile">
                                                    </div>
                                                    <div class="col-md-5 mb-3">
                                                        <label for="Com_EmailId" class="form-label">Email</label>
                                                        <input type="text" placeholder="Enter Company Email" class="form-control" id="Com_EmailId">
                                                    </div>
                                                    <div class="col-md-3 mb-3">
                                                        <label for="CompanyGST" class="form-label">Company GST</label>
                                                        <input type="text" class="form-control" placeholder="Enter Company GST No" id="CompanyGST" />
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="Pincode" class="form-label">Pincode</label>
                                                        <input type="text" onkeydown="NumberOnly(event)" class="form-control" placeholder="Enter 6 digits Pincode" maxlength="6" autocomplete="off" id="Pincode">
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="City" class="form-label">City</label>
                                                        <input type="text" placeholder="Enter Company City" class="form-control" id="City">
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="State" class="form-label">State</label>
                                                        <input type="text" class="form-control" placeholder="Enter Company State" id="State">
                                                    </div>
                                                    <div class="col-md-7 mb-3">
                                                        <label for="CompleteAddress" class="form-label">Complete Address</label>
                                                        <input type="text" placeholder="Enter Company Address" class="form-control" id="CompleteAddress">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="card">
                                        <div class="forms">
                                            <div class="form-header">
                                                <h4>Payment Details
                                                    <span class="angles"><i class="bi bi-caret-down-fill"></i></span>
                                                </h4>
                                            </div>
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-4 mb-3">
                                                        <label for="PerUnitPrice" class="form-label">Per Unit Price</label>
                                                        <input type="text" onkeydown="NumberOnly(event)" class="form-control" placeholder="Enter Price Per Unit" maxlength="10" autocomplete="off" id="PerUnitPrice" />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="card">
                            <div class="table-responsive">
                                <table id="Alluploadeddata" class="table table-borderless datatable">
                                    <thead>
                                        <tr>
                                            <th>Company</th>
                                            <th>Details</th>
                                            <th>Address</th>
                                            <th>Total Device</th>
                                            <th>Dealer Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Company name</td>
                                            <td>
                                                <h6><strong>Dealer Name (RDM001)</strong></h6>
                                                <div class="ContactNumber">9876543210,9876543210</div>
                                            </td>
                                            <td>
                                                <div class="DistributorDetailsBox">
                                                    <div class="CompleteAddress">
                                                        Maruthi Illam, 7, Pavalamalli Street,
                                            Poonga Nagar, Tiruvallur - 602 001.
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="values">200</div>
                                            </td>
                                            <td>
                                                <div class="values">1200</div>
                                            </td>


                                        </tr>
                                        <tr>
                                            <td>Company name</td>
                                            <td>
                                                <h6><strong>Dealer Name (RDM001)</strong></h6>
                                                <div class="ContactNumber">9876543210,9876543210</div>
                                            </td>
                                            <td>
                                                <div class="DistributorDetailsBox">
                                                    <div class="CompleteAddress">
                                                        Maruthi Illam, 7, Pavalamalli Street,
                                            Poonga Nagar, Tiruvallur - 602 001.
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="values">200</div>
                                            </td>
                                            <td>
                                                <div class="values">1200</div>
                                            </td>


                                        </tr>
                                        <tr>
                                            <td>Company name</td>
                                            <td>
                                                <h6><strong>Dealer Name (RDM001)</strong></h6>
                                                <div class="ContactNumber">9876543210,9876543210</div>
                                            </td>
                                            <td>
                                                <div class="DistributorDetailsBox">
                                                    <div class="CompleteAddress">
                                                        Maruthi Illam, 7, Pavalamalli Street,
                                            Poonga Nagar, Tiruvallur - 602 001.
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="values">200</div>
                                            </td>
                                            <td>
                                                <div class="values">1200</div>
                                            </td>


                                        </tr>
                                    </tbody>

                                </table>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Jquerylinks" runat="Server">
    <script src="js/admin-add-distributor.js"></script>
</asp:Content>

