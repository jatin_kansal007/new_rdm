﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="Admin_Dashboard" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="pagetitle">
        <h1>Dashboard</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="Dashboard">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </nav>

    </div>

    <section class="section dashboard">
        <div class="container">
            <div class="row">

                <div class="col-lg-12">

                    <div class="row">
                        <!-- Sales Card -->
                        <div class="col-xxl-3 col-md-3">
                            <div id="viewalluploaddata" onclick="getDataTable('alldata')" class="card info-card sales-card">
                                <div class="card-body">
                                    <h5 class="card-title">Total SIM</h5>

                                    <div class="d-flex align-items-center">
                                        <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                            <i class="bi bi-cpu"></i>
                                        </div>
                                        <div class="ps-3">
                                            <h6 id="Total_sim_count">0</h6>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- End Sales Card -->

                        <!-- Revenue Card -->
                        <div class="col-xxl-3 col-md-3">
                            <div class="card info-card revenue-card">

                                <div class="card-body">
                                    <h5 class="card-title">Total Device</h5>

                                    <div class="d-flex align-items-center">
                                        <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                            <i class="bi bi-motherboard"></i>
                                        </div>
                                        <div class="ps-3">
                                            <h6>0</h6>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- End Revenue Card -->

                        <!-- Customers Card -->
                        <div class="col-xxl-3 col-md-3">

                            <div class="card info-card customers-card">

                                <div class="card-body">
                                    <h5 class="card-title">Distributor</h5>

                                    <div class="d-flex align-items-center">
                                        <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                            <i class="bi bi-people"></i>
                                        </div>
                                        <div class="ps-3">
                                            <h6>0</h6>
                                            <span class="text-muted small pt-2 ps-1">Total Device</span> <span class="text-danger small pt-1 fw-bold">0</span>

                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <!-- End Customers Card -->
                        <!-- Customers Card -->
                        <div class="col-xxl-3  col-md-3">

                            <div class="card info-card customers-card">

                                <div class="card-body">
                                    <h5 class="card-title">Dealer</h5>

                                    <div class="d-flex align-items-center">
                                        <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                            <i class="bi bi-people"></i>
                                        </div>
                                        <div class="ps-3">
                                            <h6>0</h6>
                                            <span class="text-muted small pt-2 ps-1">Total Device</span> <span class="text-danger small pt-1 fw-bold">0</span>

                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <!-- End Customers Card -->
                    </div>
                    <div class="card">
                        <div class="row">
                            <!-- Revenue Card -->
                            <div class="col-xxl-5 col-md-5">
                                <div class="card info-card revenue-card mb-0">

                                    <div class="card-body">
                                        <h5 class="card-title">Upload File</h5>
                                        <div id="divUploaded" style="display: none;"><span>Last Uploaded:</span><span id="LastUploadedDate">04-Jan-2021</span></div>
                                        <div class="d-flex align-items-center">
                                            <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                                <i class="bi bi-file-earmark-excel-fill"></i>
                                            </div>
                                            <div class="ps-3 w-100 uploaddivclass">
                                                <div class="mb-3">
                                                    <select class="form-select" aria-label="Default select example">
                                                        <option selected="" value="Device number updation">Sensorise master file</option>
                                                        <option value="Device number updation">Device number updation</option>
                                                         
                                                    </select>
                                                    <br />
                                                    <input class="form-control" onchange="imageCheck(event,$(this));" accept=".csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" type="file" id="formFile">

                                                    <button id="processdata__btn1" onclick="ProcessData()" disabled="disabled" class="btn btn-primary BtnStyle btn-sm">Process Data</button>
                                                    <button id="fileupload__btn1" onclick="uploadFile()" class="btn btn-primary BtnStyle
                                                        btn-sm btn-primary uploadfilebtn">
                                                        Upload</button>
                                                </div>


                                            </div>

                                        </div>
                                        <%--<div style="float: right; margin-top: 5px;">
                                           
                                             
                                        </div>--%>
                                    </div>

                                </div>
                            </div>
                            <!-- End Revenue Card -->
                            <!-- Sales Card -->
                            <div class="col-xxl-7 col-md-7">
                                <div class="row">
                                    <div class="col-xxl-4 col-md-4">
                                        <div class="card info-card sales-card mb-0 mt8">

                                            <div class="card-body">
                                                <h5 class="card-title">New Records</h5>

                                                <div class="d-flex align-items-center">
                                                    <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                                        <i class="bi bi-file-earmark-bar-graph"></i>
                                                    </div>
                                                    <div class="ps-3">
                                                        <h6 id="newrecorddisplay">0</h6>
                                                        <button id="viewnewrecord" onclick="getNewRecords()" style="position: absolute; right: 10px;" class="btn btn-sm  btn-primary  BtnStyle">View</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-xxl-4 col-md-4">
                                        <div class="card info-card revenue-card mb-0 mt8">

                                            <div class="card-body">
                                                <h5 class="card-title">Mismatch Records</h5>

                                                <div class="d-flex align-items-center">
                                                    <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                                        <i class="bi bi-file-earmark-bar-graph"></i>
                                                    </div>
                                                    <div class="ps-3">
                                                        <h6 id="mismatchrecorddisplay">0</h6>
                                                        <button style="position: absolute; right: 10px;" onclick="getDataTable('mismatchdata')" id="mismatchdataget" class="btn btn-sm  btn-primary BtnStyle">View</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-xxl-4 col-md-4">

                                        <div class="card info-card customers-card mb-0 mt8">

                                            <div class="card-body">
                                                <h5 class="card-title">Same Records</h5>

                                                <div class="d-flex align-items-center">
                                                    <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                                        <i class="bi bi-file-earmark-bar-graph"></i>
                                                    </div>
                                                    <div class="ps-3">
                                                        <h6 id="samerecorddisplay">0</h6>
                                                        <button style="position: absolute; right: 10px;" onclick="getDataTable('samedata')" id="appendsamerecord" class="btn  btn-primary  btn-sm BtnStyle">View</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>




                        </div>
                    </div>

                    <div class="row">

                        <div class="col-12">
                            <div class="card">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <div onclick="location.href='OrdersForMe'" class="alert alert-info" role="alert">
                                            <span>You have 3 new Sales requests</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 text-center">
                                        <button id="AddnewRequest" type="button" data-bs-toggle="modal"
                                            data-bs-target="#NewRequestModal"
                                            class="btn btn-primary BtnStyle float-end mt-2">
                                            Add New Request</button>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-12">
                            <div id="table_card" class="card recent-sales">
                                <div class="card-body">
                                    <h5 class="card-title"><strong id="cardtitle">Recent Sales</strong> <span>| Today</span>
                                        <button id="Processbtn" class="btn btn-sm btn-primary BtnStyle">Process</button>
                                        <button id="Processbtnallnewrecordbtn" class="btn btn-sm btn-primary BtnStyle">Process all</button>
                                        <button id="rejectselectedbtn" class="btn btn-sm BtnStyle btn-danger" style="float: right;">Reject</button>
                                    </h5>
                                    <div class="table-responsive">
                                        <table id="Alluploadeddata" class="table table-borderless datatable">
                                            <thead>
                                                <tr>
                                                    <%-- <th>#</th>--%>
                                                    <th>SIMNO</th>
                                                    <th>CARDSTATE</th>
                                                    <th>CARDSTATUS</th>
                                                    <th>CUSTOMERNAME</th>
                                                    <th>ACCOUNTNO</th>
                                                    <th>ORDERNO</th>
                                                    <th>PRODUCT</th>
                                                    <th>PROJECT</th>
                                                    <th>SMSUSAGE</th>
                                                    <th>DATAUSAGE</th>
                                                    <th>IMEI</th>
                                                    <th>BOOTSTRAPPRIMARYIMSI</th>
                                                    <th>BOOTSTRAPPRIMARYTSP</th>
                                                    <th>BOOTSTRAPPRIMARYMSISDN</th>
                                                    <th>BOOTSTRAPPRIMARYSUBSCRIPTIONSTATUS</th>
                                                    <th>BOOTSTRAPFALLBACKIMSI</th>
                                                    <th>BOOTSTRAPFALLBACKTSP</th>
                                                    <th>BOOTSTRAPFALLBACKMSISDN</th>
                                                    <th>BOOTSTRAPFALLBACKSUBSCRIPTIONSTATUS</th>
                                                    <th>DATEOFCHANGEOVERTOCOMMERCIALPLAN</th>
                                                    <th>CARDENDDATE</th>
                                                    <th>COMMERCIALPRIMARYIMSI</th>
                                                    <th>COMMERCIALPRIMARYTSP</th>
                                                    <th>COMMERCIALPRIMARYMSISDN</th>
                                                    <th>COMMERCIALPRIMARYSUBSCRIPTIONSTATUS</th>
                                                    <th>LASTSRNUMBER</th>
                                                    <th>LASTSRACTION</th>
                                                    <th>LASTSRDATE</th>
                                                </tr>
                                            </thead>

                                        </table>
                                    </div>
                                </div>


                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </section>
    <div class="modal fade" id="NewRequestModal" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Qty for order device</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="form-floating mb-3">
                        <input list="browsers" type="text" class="form-control" id="floatingInputDistributor" placeholder="0">
                        <label for="floatingInputDistributor">Select Distributor</label>
                        <datalist id="browsers">
                            <option value="Distributor Name-0987654321">
                            <option value="Distributor Name-0987654321">
                            <option value="Distributor Name-0987654321">
                            <option value="Distributor Name-0987654321">
                            <option value="Distributor Name-0987654321">
                        </datalist>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="number" class="form-control" id="floatingInputQty" placeholder="0">
                        <label for="floatingInputQty">Enter Qty</label>
                    </div>
                    <div class="buttons">
                        <button id="SubmitOrderQty" class="BtnStyle btn btn-primary">Proceed</button>
                    </div>

                </div>

            </div>
        </div>
    </div>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="Jquerylinks" runat="Server">
    <script src="../jquery/adminhelper.js"></script>
</asp:Content>
