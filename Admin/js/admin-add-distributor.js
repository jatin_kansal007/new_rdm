﻿$("#OpenDownBtn").click(function (e) {
    e.stopImmediatePropagation();
    e.preventDefault();
    $(this).hide();

    if ($(".FormBody__").hasClass("close")) {
        $(".FormBody__").slideDown('slow');
        $(".FormBody__").removeClass("close");
        $(".FormBody__").addClass("open");
        $("#UserType").show('slow');
        $("#Distributor_submitbtn").show('slow');
        $("#Cancelbtn").show('slow');
        $("#ResetFields").show('slow');
        UserType
    } else {
        $(".FormBody__").slideUp('slow');
        $(".FormBody__").addClass("close");
        $(".FormBody__").removeClass("open");
        $("#Distributor_submitbtn").hide('slow');
        $("#Cancelbtn").hide('slow');
        $("#UserType").hide('slow');
        $("#ResetFields").hide('slow');
    }

});
var link = window.location.origin;
$('#Cancelbtn').click(function (e) {
    e.stopImmediatePropagation();
    $(".FormBody__").slideUp('slow');
    $(".FormBody__").addClass("close");
    $(".FormBody__").removeClass("open");
    $("#Distributor_submitbtn").hide('slow');
    $("#Cancelbtn").hide('slow');
    $("#UserType").hide('slow');
    $("#ResetFields").hide('slow');
    $("#OpenDownBtn").show();
})

function ResetFields() {
    $('.FormBody__').find('input').val('');
    $('#Distributor_submitbtn').removeAttr('user_id');
}

$(function () {
    getDistributorList();
});

$(".form-header").click(function (e) {
    e.stopImmediatePropagation();
    e.preventDefault();
    $(this).parent().find(".form-body").slideToggle();
    if ($(this).find('.angles i').hasClass('bi-caret-down-fill')) {
        $(this).find('.angles').empty();
        $(this).find('.angles').append('<i class="bi bi-caret-up-fill"></i>');
    } else {
        $(this).find('.angles').empty();
        $(this).find('.angles').append('<i class="bi bi-caret-down-fill"></i>');
    }

});

function SaveDistributorData(Current) {
    var ActionUserId = getNullOrEmpty(Current.attr('user_id'));
    var Name = $.trim($('#DealerName').val());
    var Mobile = $.trim($('#Mobile').val());
    var AltMobile = $.trim($('#AlternateMobile').val());
    var Email = $.trim($('#EmailId').val());
    var UserType = $('#UserType option:selected').val();
    var CompanyName = $.trim($('#CompanyName').val());
    var CompanyMobile = $.trim($('#Com_Mobile').val());
    var CompanyAltMobile = $.trim($('#Com_AlternateMobile').val());
    var CompanyEmail = $.trim($('#Com_EmailId').val());
    var CompanyPincode = $.trim($('#Pincode').val());
    var CompanyCity = $.trim($('#City').val());
    var CompanyState = $.trim($('#State').val());
    var CompanyAddress = $.trim($('#CompleteAddress').val());
    var CompanyGstNo = $.trim($('#CompanyGST').val());
    var PricePerUnit = $.trim($('#PerUnitPrice').val());
    var Url = link + '/Admin/AddNewDistributor.aspx/SaveDistributorData';
    var Data = JSON.stringify({
        ActionUserId: ActionUserId, Name: Name, Mobile: Mobile, AltMobile: AltMobile,
        Email: Email, UserType: UserType, CompanyName: CompanyName, CompanyMobile: CompanyMobile,
        CompanyAltMobile: CompanyAltMobile, CompanyEmail: CompanyEmail, CompanyPincode: CompanyPincode,
        CompanyCity: CompanyCity, CompanyState: CompanyState, CompanyAddress: CompanyAddress,
        CompanyGstNo: CompanyGstNo, PricePerUnit: PricePerUnit
    })
    var Result = doAjax(Url, Data);
    if (Result != '') {
        swal('Data Alert', Result, 'error');
    }
    else {
        swal('Data Alert', 'Data saved successfully', 'success');
        ResetFields();
        getDistributorList();
        if (ActionUserId != '') {
            $("#OpenDownBtn").trigger('click');
        }
    }
}

function getDistributorList() {
    var Url = link + '/Admin/AddNewDistributor.aspx/getDistributorList';
    var Data = {};
    var Result = doAjax(Url, Data);
    if (Result != '') {
        var parsedData = JSON.parse(Result).table;
        $('#Alluploadeddata thead').empty();
        $('#Alluploadeddata tbody').empty();
        var tr = '';
        for (var i = 0; i < parsedData.length; i++) {
            tr = '<tr><td>' + parsedData[i].companyname
                + '</td><td>' + parsedData[i].name + ' - ' + parsedData[i].mobile
                + '</td><td>' + parsedData[i].companyaddress
                + '</td><td>' + parsedData[i].usertype
                + '</td><td>' + parsedData[i].totaldevice
                + '</td><td>' + parsedData[i].perunitprice
                + '</td><td><i class="bi bi-pencil-square" onclick="editUser(' + parsedData[i].userid + ')"></i></td></tr>'
        }
        if (tr != '') {
            var th = '<tr><th>Company</th><th>Details</th><th>Address</th>' +
                '<th>Type</th><th>Total Device</th><th>Price/Unit</th><th>Edit</th></tr>';
            $('#Alluploadeddata thead').append(th);
            $('#Alluploadeddata tbody').append(tr);
        }

        if ($.fn.dataTable.isDataTable('#Alluploadeddata')) {
            table = $('#dtTable').DataTable({
                dom: 'lBfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        }
        else {
            table = $('#Alluploadeddata').DataTable({
                paging: false,
                searching: false,
                dom: 'lBfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        }
  
    }
}

function editUser(UserId) {
    ResetFields();
    var Url = link + '/Admin/AddNewDistributor.aspx/getDistributorByUserId';
    var Data = JSON.stringify({ UserId: UserId });
    var Result = doAjax(Url, Data);
    $('#Distributor_submitbtn').attr('user_id', UserId);
    var result1 = JSON.parse(Result).table;
    var result2 = JSON.parse(Result).table1;
    $('#DealerName').val(result1[0].name);
    $('#Mobile').val(result1[0].mobile);
    $('#AlternateMobile').val(result1[0].alternatemobile);
    $('#EmailId').val(result1[0].emailid);

    $('#CompanyName').val(result2[0].companyname);
    $('#Com_Mobile').val(result2[0].companymobile);
    $('#Com_AlternateMobile').val(result2[0].companyaltmobile);
    $('#Com_EmailId').val(result2[0].companyemail);
    $('#CompanyGST').val(result2[0].companygstno);
    $('#Pincode').val(result2[0].companypincode);
    $('#City').val(result2[0].companycity);
    $('#State').val(result2[0].companystate);
    $('#CompleteAddress').val(result2[0].companyaddress);
    $('#PerUnitPrice').val(result2[0].perunitprice);
    $('UserType option[value="' + result2[0].usertype + '"]').attr("selected", true);

    $("#OpenDownBtn").trigger('click');
    //$('#OpenDownBtn').show('slow');
}