﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="OrdersForMe.aspx.cs" Inherits="Admin_OrdersForMe" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="pagetitle">
        <h1>Orders</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="Dashboard">Home</a></li>
                <li class="breadcrumb-item active">Orders</li>
            </ol>
        </nav>

    </div>
    <!-- End Page Title -->

    <section class="section dashboard">
        <div class="container">
            <div class="row">
                <%--  <div class="col-lg-12">
                     <button id="viewalluploaddata" class="btn btn-sm btn-success">View All Data</button>
                        </div>--%>
                <!-- Left side columns -->
                <div class="col-lg-12">

                    <div class="row">
                        <!-- Sales Card -->
                        <div class="col-xxl-3 col-md-3">
                            <div id="viewalluploaddata" onclick="getDataTable('alldata')" class="card info-card sales-card">
                                <div class="card-body">
                                    <h5 class="card-title">Total SIM</h5>

                                    <div class="d-flex align-items-center">
                                        <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                            <i class="bi bi-cpu"></i>
                                        </div>
                                        <div class="ps-3">
                                            <h6 id="Total_sim_count">0</h6>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- End Sales Card -->

                        <!-- Revenue Card -->
                        <div class="col-xxl-3 col-md-3">
                            <div class="card info-card revenue-card">

                                <div class="card-body">
                                    <h5 class="card-title">Total Device</h5>

                                    <div class="d-flex align-items-center">
                                        <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                            <i class="bi bi-motherboard"></i>
                                        </div>
                                        <div class="ps-3">
                                            <h6>0</h6>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- End Revenue Card -->

                        <!-- Customers Card -->
                        <div class="col-xxl-3 col-md-3">

                            <div class="card info-card customers-card">

                                <div onclick="location.href='AddNewDistributor'" class="card-body">
                                    <h5 class="card-title">Distributor</h5>

                                    <div class="d-flex align-items-center">
                                        <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                            <i class="bi bi-people"></i>
                                        </div>
                                        <div class="ps-3">
                                            <h6>0</h6>
                                            <span class="text-muted small pt-2 ps-1">Total Device</span> <span class="text-danger small pt-1 fw-bold">0</span>

                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <!-- End Customers Card -->
                        <!-- Customers Card -->
                        <div class="col-xxl-3  col-md-3">

                            <div class="card info-card customers-card">

                                <div class="card-body">
                                    <h5 class="card-title">Dealer</h5>

                                    <div class="d-flex align-items-center">
                                        <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                            <i class="bi bi-people"></i>
                                        </div>
                                        <div class="ps-3">
                                            <h6>0</h6>
                                            <span class="text-muted small pt-2 ps-1">Total Device</span> <span class="text-danger small pt-1 fw-bold">0</span>

                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <!-- End Customers Card -->
                    </div>


                    <div class="row">
                        <!-- Recent Sales -->
                        <div class="col-12">
                            <div id="" class="card recent-sales">
                                <div class="card-body">
                                    <h5 class="card-title"><strong id="cardtitle">Sales Request</strong> <span>| Today</span>

                                    </h5>
                                    <div class="table-responsive">
                                        <table id="AllRequestOrders" class="table table-borderless datatable">
                                            <thead>
                                                <tr>
                                                    <%-- <th>#</th>--%>
                                                    <th>Sr No.</th>
                                                    <th></th>
                                                    <th>User Type</th>
                                                    <th>Details</th>
                                                    <th>Date</th>
                                                    <th>Request QTY</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>
                                                        <button class="btn btn-sm btn-primary AllotDevice BtnStyle">Allocate Device</button></td>
                                                    <td>Distributor</td>
                                                    <td>
                                                        <div class="DistributorDetails">
                                                            <div class="ProfileImg">

                                                                <img src="../assets/img/man.png" />
                                                            </div>
                                                            <div class="OtherDetails">
                                                                <h5>Distributor Name (RDM001)</h5>
                                                                <div>9876543210</div>
                                                                <div>C - Wing, Suyog Co.Housing Society Ltd, T. P.S. Road & III Link Road, Vazira, Borivali, West Mumbai, Maharashtra, 400092</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><span class="OrderDate">08-Jan-2022</span></td>
                                                    <td><i class="bi bi-sim-fill"></i> 200</td>
                                                </tr>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <!-- End Recent Sales -->
                    </div>
                </div>
            </div>
            <!-- End Left side columns -->
        </div>
        <div class="modal-div" draggable="true">
            <div class="modal-div-content" draggable="true">
                <div class="modal-div-header">
                    <h5>Allocate Device</h5>

                </div>
                <div class="modal-div-body">
                    <div class="row">
                      <div class="col-sm-12">
                          <button style="float:right;" class="btn btn-success BtnStyle">Allocate Device</button>
                      </div>  
                    </div>
                    <br />
                    <div class="table-responsive">
                        <table id="" class="table table-borderless datatable">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Sr No.</th>
                                    <th>ICC ID</th>
                                    <th>IMEI Number</th>
                                    <th>Device Sr no</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="checkbox" checked="checked"/></td>
                                    <td>1</td>
                                    <td><i class="bi bi-sim-fill"></i> 543434546575645645</td>
                                    <td><i class="bi bi-sim-fill"></i> 54343454657564 </td>
                                    <td><i class="bi bi-sim-fill"></i> 435345435435</td>
                                    <td><div class="devicestatus">Active</div></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" checked="checked"/></td>
                                    <td>2</td>
                                    <td><i class="bi bi-sim-fill"></i> 543434546575645645</td>
                                    <td><i class="bi bi-sim-fill"></i> 54343454657564 </td>
                                    <td><i class="bi bi-sim-fill"></i> 435345435435</td>
                                    <td><div class="devicestatus">Active</div></td>
                                </tr>
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>
        </div>
    </section>



</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="Jquerylinks" runat="Server">
    <script src="js/distributor-orders.js"></script>
</asp:Content>
