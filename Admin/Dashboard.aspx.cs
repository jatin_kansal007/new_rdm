﻿using ExcelDataReader;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

public partial class Admin_Dashboard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
        {
            CreateSession();
        }
        else
        {
            Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath + "/Login.aspx");
        }
    }

    private void CreateSession()
    {
        try
        {
            string Qstring = Request.QueryString.ToString();
            string QueryString = PasswordEncryptDecrypt.DecryptString(HttpUtility.UrlDecode(Qstring));
            string UserName = HttpUtility.ParseQueryString(QueryString).Get("username").Trim();
            string Password = HttpUtility.ParseQueryString(QueryString).Get("password").Trim();
            string Token = HttpUtility.ParseQueryString(QueryString).Get("token").Trim();
            if (UtilityModule.checkUserAuth(UserName, Password, Token))
            {
                cl_login_user cl = new cl_login_user();
                DataSet ds = new DataSet();
                cl.username = UserName;
                cl.password = Password;
                cl.Token = Token;
                cl.Mode = "getUserDetails";
                ds = cl.verifyToken();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    string UserDetails = HttpUtility.UrlEncode(PasswordEncryptDecrypt.EncryptString("userid=" + ds.Tables[0].Rows[0]["userid"].ToString() +
                       "&partnerid=" + ds.Tables[0].Rows[0]["partnerid"].ToString() +
                       "&usertype=" + ds.Tables[0].Rows[0]["usertype"].ToString()));
                    if (Request.Cookies["checkCookie"] == null)
                    {
                        Session["UserD"] = UserDetails;
                        Session["UserC"] = QueryString;
                    }
                    else
                    {
                        HttpCookie UserD = new HttpCookie("UserD");
                        UserD.Value = UserDetails;  // Case sensitivity
                        UserD.Expires = DateTime.Now.AddDays(365);
                        HttpContext.Current.Response.Cookies.Add(UserD);

                        HttpCookie UserC = new HttpCookie("UserC");
                        UserC.Value = Qstring;  // Case sensitivity
                        UserC.Expires = DateTime.Now.AddDays(365);
                        HttpContext.Current.Response.Cookies.Add(UserC);
                    }

                }
                else
                {
                    Response.Redirect(UtilityModule.getLoginPageUrl());
                }
            }
        }
        catch (Exception ex)
        {
            new ExceptionLogging(ex);
            Response.Redirect(UtilityModule.getLoginPageUrl());
        }

    }

    [WebMethod]
    public static string UploadExcelFile(string Base64, string Header)
    {
        string Result = "";
        try
        {
            if (UtilityModule.getUserTokenCheck())
            {
                if (UtilityModule.CheckExcelFileMimeType(Header))
                {
                    string FileExt = UtilityModule.getExcelMimeName(Header);
                    string filepath = HttpContext.Current.Server.MapPath("~/MyExcel/TempFile/");  //Text File Path
                    if (!System.IO.Directory.Exists(filepath))
                    {
                        System.IO.Directory.CreateDirectory(filepath);
                    }
                    filepath = filepath + DateTime.Now.ToString("ddMMMyyyyhhmmss") + "." + FileExt;
                    var ExcelFile = Base64.Split(',')[1];
                    byte[] fileAsBytes = Convert.FromBase64String(ExcelFile);
                    File.WriteAllBytes(filepath, fileAsBytes);
                    string ExcelFileReadPath = "";
                    if (FileExt != "csv")
                    {
                        using (var stream = new FileStream(filepath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                        {
                            IExcelDataReader reader = null;
                            if (filepath.EndsWith(".xls"))
                            {
                                reader = ExcelReaderFactory.CreateBinaryReader(stream);
                            }
                            else if (filepath.EndsWith(".xlsx"))
                            {
                                reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                            }

                            if (reader == null)
                            {
                                Result = "Please attach excel";
                                return Result;
                            }


                            var ds = reader.AsDataSet(new ExcelDataSetConfiguration()
                            {
                                ConfigureDataTable = (tableReader) => new ExcelDataTableConfiguration()
                                {
                                    UseHeaderRow = false
                                }
                            });

                            var csvContent = string.Empty;
                            int row_no = 0;
                            while (row_no < ds.Tables[0].Rows.Count)
                            {
                                var arr = new List<string>();
                                for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                                {
                                    arr.Add(ds.Tables[0].Rows[row_no][i].ToString().Replace(",", "&#44;"));
                                }
                                row_no++;
                                csvContent += string.Join(",", arr) + "\n";
                            }
                            string destinationCsvFilePath = HttpContext.Current.Server.MapPath("~/MyExcel/PerFile/");  //Text File Path
                            if (!System.IO.Directory.Exists(destinationCsvFilePath))
                            {
                                System.IO.Directory.CreateDirectory(destinationCsvFilePath);
                            }
                            destinationCsvFilePath = destinationCsvFilePath + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".csv";
                            ExcelFileReadPath = destinationCsvFilePath;
                            StreamWriter csv = new StreamWriter(destinationCsvFilePath, false);
                            csv.Write(csvContent);
                            csv.Close();
                        }
                    }
                    else
                    {
                        ExcelFileReadPath = HttpContext.Current.Server.MapPath("~/MyExcel/PerFile/");  //Text File Path
                        if (!System.IO.Directory.Exists(ExcelFileReadPath))
                        {
                            System.IO.Directory.CreateDirectory(ExcelFileReadPath);
                        }
                        var file = File.ReadLines(filepath).ToArray();
                        var csvContent = string.Empty;
                        foreach (string line in file)
                        {
                            if (String.IsNullOrWhiteSpace(line) == false)
                            {
                                //bool quotesStarted = false;
                                // StringBuilder sbLine = new StringBuilder();
                                TextFieldParser parser = new TextFieldParser(new StringReader(line));
                                parser.HasFieldsEnclosedInQuotes = true;
                                parser.SetDelimiters(",");
                                string[] fields;

                                while (!parser.EndOfData)
                                {
                                    var arr = new List<string>();
                                    fields = parser.ReadFields();
                                    foreach (string field in fields)
                                    {
                                        arr.Add(field.Replace(",", "&#44;"));
                                        //sbLine.Append(field.Replace(",", "&#44;"));
                                    }
                                    csvContent += string.Join(",", arr) + "\n";
                                }
                                parser.Close();

                            }

                        }
                        ExcelFileReadPath = ExcelFileReadPath + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".csv";
                        StreamWriter csv = new StreamWriter(ExcelFileReadPath, false);
                        csv.Write(csvContent);
                        csv.Close();

                        //File.WriteAllBytes(ExcelFileReadPath, fileAsBytes);
                    }

                    var lines = File.ReadAllLines(ExcelFileReadPath);
                    string[] headers = lines[0].Split(',').Select(x => x.Trim('\"')).ToArray();

                    var xml = new XElement("TopElement",
                       lines.Where((line, index) => index > 0).Select(line => new XElement("Item",
                          line.Split(',').Select((column, index) => new XElement(headers[index], column)))));

                    if (File.Exists(filepath))
                    {
                        File.Delete(filepath);
                    }
                    cl_Admin ca = new cl_Admin();
                    DataSet dsd = new DataSet();
                    ca.UserId = UtilityModule.getParamFromUrl("userid");
                    ca.ExcelSheet = xml.ToString();
                    ca.Mode = "InsertExcelFile";
                    dsd = ca.fnAdminUploadExcel();
                }
            }
            else
            {
                Result = "Invalid User";
                return Result;
            }
        }
        catch (Exception ex)
        {
            new ExceptionLogging(ex);
        }

        return Result;
    }

    [WebMethod]
    public static void ProcessData()
    {
        try
        {
            if (UtilityModule.getUserTokenCheck())
            {
                cl_Admin ca = new cl_Admin();
                DataSet ds = new DataSet();
                ca.UserId = UtilityModule.getParamFromUrl("userid");
                ca.Mode = "ProcessData";
                ds = ca.fnAdminUploadExcel();
            }
        }
        catch (Exception ex)
        {
            new ExceptionLogging(ex);
        }
    }

    [WebMethod]
    public static string getPrcessData()
    {
        string Result = "";
        try
        {
            if (UtilityModule.getUserTokenCheck())
            {
                cl_Admin ca = new cl_Admin();
                DataSet ds = new DataSet();
                ca.UserId = UtilityModule.getParamFromUrl("userid");
                ca.Mode = "AdminPageData";
                ds = ca.fnAdminUploadExcel();
                if (ds != null && ds.Tables.Count > 0)
                {
                    Result = LowercaseJsonSerializer.SerializeObject(ds);
                    return Result;
                }
            }
        }
        catch (Exception ex)
        {
            new ExceptionLogging(ex);
        }

        return Result;
    }



    [WebMethod]
    public static string method_upload_excel_file(List<CL_EXCEL_DATA_UPLOAD_LIST2> data, string Original_File_Name)
    {
        string result = "";
        try
        {
            XmlSerializer serializer = new XmlSerializer(data.GetType());
            StringWriter str = new StringWriter();
            using (XmlWriter writer = XmlWriter.Create(str, new XmlWriterSettings { OmitXmlDeclaration = true }))
            {
                serializer.Serialize(writer, data);
                cl_uploadxlsxfile UF = new cl_uploadxlsxfile();
                UF.XMLData = str.ToString();
                UF.Original_File_Name = Original_File_Name;
                UF.User_ID = UtilityModule.getParamFromUrl("userid");
                UF.usefor = "FILE_UPLOAD_IN_TL";
                DataSet ds = UF.uploadExcelData();
                if (ds != null)
                {
                    result = ds.Tables[0].Rows[0]["RESULT"].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            new ExceptionLogging(ex);

        }
        return result;
    }




    //[WebMethod]

    //public static void method_frouploadfile(string data)
    //{
    //    try
    //    {
    //        if (!string.IsNullOrEmpty(data))
    //        {
    //            //data = data.Remove(0, 78);


    //            string path = HttpContext.Current.Server.MapPath("~/MyExcel");
    //            if (!System.IO.Directory.Exists(path))
    //            {
    //                System.IO.Directory.CreateDirectory(path);
    //            }
    //            long n = long.Parse(DateTime.Now.ToString("yyyyMMddHHmmss"));

    //            string Path = HttpRuntime.AppDomainAppPath + "MyExcel" + "\\" + Convert.ToString(n) + ".csv";
    //            if (File.Exists(Path))
    //            {
    //                File.Delete(Path);
    //            }
    //            using (FileStream fs = new FileStream(Path, FileMode.Create))
    //            {
    //                using (BinaryWriter bw = new BinaryWriter(fs))
    //                {
    //                    byte[] bytearray = Convert.FromBase64String(data);
    //                    bw.Write(bytearray);
    //                    bw.Close();
    //                }
    //            }

    //            new System.Threading.Thread(() =>
    //            {
    //                System.Threading.Thread.CurrentThread.IsBackground = true;
    //                using (var reader = new StreamReader(Path))
    //                {
    //                    int row = 0;
    //                    while (!reader.EndOfStream)
    //                    {
    //                        string line = HttpUtility.HtmlDecode(reader.ReadLine());

    //                        var values = line.Split(new[] { "~" }, StringSplitOptions.None);

    //                        row++;
    //                        if (row == 1)
    //                        {
    //                            continue;
    //                        }
    //                        else
    //                        {
    //                            cl_upload_files ca = new cl_upload_files(values[0], values[1], values[2],
    //                                values[3], values[4], values[5], values[6], values[7], values[8]
    //                                , values[9], values[10], values[11], values[12], values[13], values[14]
    //                                , values[15], values[16], values[17], values[18], values[19], values[20]
    //                                , values[21], values[22], values[23], values[24], values[25], values[26]
    //                                , values[27], values[28], values[29], values[30], values[31], values[32]
    //                                , values[33], values[34], values[35], values[36], values[37], values[38]
    //                                , values[39]);
    //                        }

    //                    }
    //                }

    //            }).Start();

    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        ExceptionLogging.SendErrorToText(ex);

    //    }
    //}


    //[WebMethod]

    //public static string method_process_upload_excel_file()
    //{
    //    string result = "";
    //    string MatchRecord = "", MismatchRecord = "", NewRecord = "";
    //    cl_uploadxlsxfile UF = new cl_uploadxlsxfile();
    //    UF.User_ID = cl_encrypt.Decrypt(HttpContext.Current.Request.Cookies["userid"].Value.ToString());
    //    UF.usefor = "PROCESS_DATA";
    //    DataSet ds = UF.uploadExcelData();
    //    if (ds != null)
    //    {
    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            MatchRecord = ds.Tables[0].Rows[0]["MatchRecord"].ToString() != null ? ds.Tables[0].Rows[0]["MatchRecord"].ToString() : "0";
    //        }
    //        if (ds.Tables[1].Rows.Count > 0)
    //        {
    //            MismatchRecord = ds.Tables[1].Rows[0]["MismatchRecord"].ToString() != null ? ds.Tables[1].Rows[0]["MismatchRecord"].ToString() : "0";
    //        }
    //        if (ds.Tables[2].Rows.Count > 0)
    //        {
    //            NewRecord = ds.Tables[2].Rows[0]["NewRecord"].ToString() != null ? ds.Tables[2].Rows[0]["NewRecord"].ToString() : "0";
    //        }



    //        result = MatchRecord + "~" + MismatchRecord + "~" + NewRecord;
    //    }
    //    return result;
    //}


    [WebMethod]

    public static string method_getuploadeddata(string foruse)
    {
        string Data = "";
        try
        {
            string typepass = "";
        if (foruse == "alldata")
        {
            typepass = "GET_UPLOADED_DATA";
        }
        else if (foruse == "samedata")
        {
            typepass = "GET_MATCHED_DATA";
        }
        else if (foruse == "mismatchdata")
        {
            typepass = "GET_MISMATCHED_DATA";
        }
        else if (foruse == "NewRecord")
            {
                typepass = "GET_NEW_DATA";
            }
            cl_uploadxlsxfile UF = new cl_uploadxlsxfile();
        UF.User_ID = UtilityModule.getParamFromUrl("userid");
        UF.usefor = typepass;
        DataSet ds = UF.uploadExcelData();
        if (ds != null)
        {
            Data = cl_convertdttoJSON.DataTableToJSONWithStringBuilder(ds.Tables[0]);
        }
        }
        catch (Exception ex)
        {
            new ExceptionLogging(ex);

        }
        return Data;
    }


    //[WebMethod]
    //public static string methods_getcounts()
    //{
    //    string result = string.Empty;
    //    cl_uploadxlsxfile UF = new cl_uploadxlsxfile();
    //    UF.User_ID = cl_encrypt.Decrypt(HttpContext.Current.Request.Cookies["userid"].Value.ToString());
    //    UF.usefor = "GET_TOTALS";
    //    DataSet ds = UF.uploadExcelData();
    //    if (ds != null)
    //    {
    //        result = ds.Tables[0].Rows[0]["TOTAL_SIM"].ToString();
    //    }

    //    return result;
    //}

    [WebMethod]
    public static string method_processdataintomaintable(List<cl_simno_list> SIMNo)
    {
        string result = "";
        cl_uploadxlsxfile UF = new cl_uploadxlsxfile();
        DataSet ds = new DataSet();
        DataTable dt = new DataTable("PROCESS_NEW_DATA");
        dt.Columns.Add("SIMID_OF_TL", typeof(string));
        string xml = "";
        ToDataTable(dt, SIMNo);
        using (StringWriter str = new StringWriter())
        {
            dt.WriteXml(str);
            xml = str.ToString();
        }
        UF.usefor = "UPDATE_MISMATCH_DATA";
        UF.SIMNo_TL = xml;
        UF.User_ID = UtilityModule.getParamFromUrl("userid");
        ds = UF.uploadExcelData();

        return result;
    }

    public static DataTable ToDataTable<T>(DataTable dt, IEnumerable<T> list)
    {
        Type elementType = typeof(T);
        PropertyInfo[] _props = elementType.GetProperties();

        foreach (T item in list)
        {
            DataRow row = dt.NewRow();
            foreach (PropertyInfo propInfo in _props)
            {
                row[propInfo.Name] = propInfo.GetValue(item, null) ?? DBNull.Value;
            }
            dt.Rows.Add(row);
        }
        return dt;

    }


    [WebMethod]
    public static string method_processdataAll()
    {
        string result = "";
        cl_uploadxlsxfile UF = new cl_uploadxlsxfile();
        DataSet ds = new DataSet();
        UF.usefor = "UPDATE_ALL_MISMATCH_DATA";
        UF.User_ID = UtilityModule.getParamFromUrl("userid");
        ds = UF.uploadExcelData();
        if (ds != null)
        {

        }
        return result;

    }

    [WebMethod]
    public static string method_rejectprocessdata(List<cl_simno_list> SIMNo)
    {
        string result = "";
        cl_uploadxlsxfile UF = new cl_uploadxlsxfile();
        DataSet ds = new DataSet();
        DataTable dt = new DataTable("PROCESS_NEW_DATA");
        dt.Columns.Add("SIMID_OF_TL", typeof(string));
        string xml = "";
        ToDataTable(dt, SIMNo);
        using (StringWriter str = new StringWriter())
        {
            dt.WriteXml(str);
            xml = str.ToString();
        }
        UF.usefor = "REJECT_MISMATCH_DATA";
        UF.SIMNo_TL = xml;
        UF.User_ID = UtilityModule.getParamFromUrl("userid");
        ds = UF.uploadExcelData();

        return result;
    }

    [WebMethod]
    public static string method_getallsimddata()
    {
        string Data = "";
        cl_uploadxlsxfile UF = new cl_uploadxlsxfile();
        UF.User_ID = UtilityModule.getParamFromUrl("userid");
        UF.usefor = "GET_ALL_SIM_DATA";
        DataSet ds = UF.uploadExcelData();
        if (ds != null)
        {
            Data = cl_convertdttoJSON.DataTableToJSONWithStringBuilder(ds.Tables[0]);
        }

        return Data;
    }


    [WebMethod]
    public static string NewRecordProcessData()
    {
        string result = "";

        try
        {
            cl_uploadxlsxfile UF = new cl_uploadxlsxfile();
            UF.User_ID = UtilityModule.getParamFromUrl("userid");
            UF.usefor = "InsertAllNewRecord";
            DataSet ds = UF.uploadExcelData();
            if (ds!= null)
            {
                result = ds.Tables[0].Rows[0]["result"].ToString();
            }

        }
        catch (Exception ex)
        {
            new ExceptionLogging(ex);
        }
        return result;
    }





}